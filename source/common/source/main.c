/*
 *  Legend of Pandora
 *
 *  Copyright (C) 2013 Justin Smith
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  To contact the author please email at justin@u4e.us
 */

#include <stdio.h>
#include <stdlib.h>

/* Pelles C additions */
#ifdef __POCC__
#pragma warn(disable: 2154 2231)
#pragma comment(lib, "SDL_main.lib")
#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDL_Mixer.lib")
#include <windows.h>
#ifndef _SDL_h
#include <SDL.h>
#endif
#include <SDL_main.h>
#include <SDL_Mixer\SDL_mixer.h>
#endif
/* Pelles C additions end */

/* linux */
#ifdef __linux__
#ifndef _SDL_h
#include <SDL.h>
#endif
#include <SDL_main.h>
#include <SDL_mixer.h>
#endif
/* linux */

#ifndef _definitions_h
#include "definitions.h"
#endif

#ifndef _functions_h
#include "functions.h"
#endif

#ifdef __POCC__
int WINAPI wWinMain(HINSTANCE h , HINSTANCE p , wchar_t* c , int s)
#endif
#ifdef __linux__
int main(int argc, char *argv[])
#endif
{
	// diagnosis
	//printf("Storage size for int: %d bytes \n", sizeof(int));
	//printf("Storage size for float : %d bytes\n", sizeof(float));
        
	int i = 0;    // a generic counter
	int o = 0;    // a generic counter
	int q = 0;    // a generic counter
		
		// Linux command line string storage
        char temp_string[80] = {'\0'};  // a temporary string holder

        // our game settings
        struct game_settings LoP;
        LoP.width = 800;
        LoP.height = 480;
        LoP.zoom = 4;   // multiplier for pixel size
        LoP.full = FALSE;
        LoP.tilesize = TILESIZE * LoP.zoom;
        LoP.mute = FALSE;
        LoP.track_time = FALSE;
        // int LoP.screen_depth = 16        // we should not need this
          
        // read in preferences.txt
	// (initialize this to all NULL to make Windows memory happy.)
	int prefs_text[1024] = { 0 };
	// fill the array
	get_prefs_text(prefs_text, LoP);
        // print the prefs_text
        /*
        for (i = 0; i < 1024; i++) {
                // skip extra newlines
                while ((prefs_text[i] == '\n') && (prefs_text[i+1] == '\n')) i++;
                printf("%c", prefs_text[i]);
        }
        */
        // our command line options
        /*
        printf("You started the program with these options:\n");
        for (i = 0; i < argc; i++) {
                printf("%i: %s\n", i, argv[i]);
        }
        */
        // copy the prefs_text into LoP settings if nothing was added to command line 
             
        // if argc is 1 it doesn't have any arguments so copy over the preferences.txt
		#ifdef __linux__
        if (argc == 1) 
		#endif
		{
                // scan preferences.txt for number of arguments
                for (i = 0, o = 1; i < 1024; i++) {
                        if (prefs_text[i] == '-') {
                                // o is the number of arguments indicated by the '-'
                                o++;
                        }
                }
                //printf("o is now == %i\n", o);
                if (o != 1) {   // do nothing because no commands where found
                        for (i = 0; i < 1024; i++) {
                                while ((prefs_text[i] != '-') && (i < 1024)) i++;
                                if (prefs_text[i] == '-') {
                                        // skip to the next character
                                        i++;
                                        // clear temp_string
                                        for (q = 0; q < 80; q++) {
                                                temp_string[q] = '\0';
                                        }
                                        switch (prefs_text[i]) {
                                        case 'm':
                                                LoP.mute = TRUE;
                                                //printf("Mute is on\n");
                                                break;
                                        case 't':
                                                LoP.track_time = TRUE;
                                                break;
                                        case 'f':
                                                LoP.full = TRUE;
                                                //printf("Fullscreen is on\n");
                                                break;
                                        case 'w':
                                                // skip the space
                                                i+=2;
                                                // scan through to get number
                                                for (q = 0; prefs_text[i] != '\n' && (i < 1024); i++, q++) {
                                                        if (isdigit(prefs_text[i])) {
                                                                //printf("%c", prefs_text[i]);
                                                                temp_string[q] = prefs_text[i];
                                                        }
                                                }
                                                // end the string correctly in temp_string
                                                q++;
                                                temp_string[q] = '\0';
                                                q = atoi(temp_string);
                                                //printf("%s is %d\n", temp_string, p);
                                                // set width
                                                if (q > 0) {
                                                        LoP.width = q;
                                                        //printf("width is %d\n", LoP.width);
                                                }
                                                break;
                                        case 'h':
                                                // skip the space
                                                i+=2;
                                                // scan through to get number
                                                for (q = 0; prefs_text[i] != '\n' && (i < 1024); i++, q++) {
                                                        if (isdigit(prefs_text[i])) {
                                                                //printf("%c", prefs_text[i]);
                                                                temp_string[q] = prefs_text[i];
                                                        }
                                                }
                                                // end the string correctly in temp_string
                                                q++;
                                                temp_string[q] = '\0';
                                                q = atoi(temp_string);
                                                //printf("%s is %d\n", temp_string, p);
                                                // set height 
                                                if (q > 0) {
                                                        LoP.height = q;
                                                        //printf("height is %d\n", LoP.height);
                                                }
                                                break;
                                        case 'z':
                                                // skip the space
                                                i+=2;
                                                // scan through to get number
                                                for (q = 0; prefs_text[i] != '\n' && (i < 1024); i++, q++) {
                                                        if (isdigit(prefs_text[i])) {
                                                                //printf("%c", prefs_text[i]);
                                                                temp_string[q] = prefs_text[i];
                                                        }
                                                }
                                                // end the string correctly in temp_string
                                                q++;
                                                temp_string[q] = '\0';
                                                q = atoi(temp_string);
                                                //printf("%s is %d\n", temp_string, p);
                                                // set zoom
                                                if (q > 0) {
                                                        LoP.zoom = q;
                                                        //printf("zoom is %d\n", LoP.zoom);
                                                }
                                                break;
                                        default:
                                                break;
                                        }
                                }
                        }
                }
        }
		/*
        // parse the command line options
        if (argc != 1) {        // if argc is 1 then no options were entered
                // fullscreen option
                char fullscreen_string[5] = {'-', 'f', '\0'};
                // width option
                char width_string[5] = {'-', 'w', '\0'};
                // height option
                char height_string[5] = {'-', 'h', '\0'};
                // zoom option
                char zoom_string[5] = {'-', 'z', '\0'};
                // mute option
                char mute_string[5] = {'-', 'm', '\0'};
                // track time option
                char track_time_string[5] = {'-', 't', '\0'};
 

                // string comparisons
                //
                for (i = 0; i < argc; i++) {
                        // is the string the same?
                        // fullscreen
                        if (!strncmp( argv[i], fullscreen_string, 5)) {
                                LoP.full = TRUE;
                        }
                        //printf("LoP.full = %s\n", LoP.full ? "TRUE" : "FALSE");
                        // screen width
                        if (!strncmp( argv[i], width_string, 5)) {
                                LoP.width = atoi(argv[i + 1]);
                                if (LoP.width < 200) {
                                        LoP.width = 200 * LoP.zoom;
                                        printf("Invalid screen width\n");
                                }
                        }
                        //printf("width is: %i\n", LoP.width);
                        // screen height
                        if (!strncmp( argv[i], height_string, 5)) {
                                LoP.height = atoi(argv[i + 1]);
                                if (LoP.height < 120) {
                                        LoP.height = 120 * LoP.zoom;
                                        printf("Invalid screen height\n");
                                }
                        }
                        //printf("height is: %i\n", LoP.height);
                        // zoom
                        if (!strncmp( argv[i], zoom_string, 5)) {
                                LoP.zoom = atoi(argv[i + 1]);
                                if (LoP.zoom < 1 || 
                                    LoP.zoom > 8) {
                                        LoP.zoom = 4;
                                        printf("Invalid screen zoom\n");
                                }
                        }
                        //printf("zoom is: %i\n", LoP.zoom);
                        // set the tilesize based on zoom
                        LoP.tilesize = TILESIZE * LoP.zoom;
                        // Zoom resolution check to make sure rez is big enough
                        if (LoP.width < (LoP.zoom * 200)) {
                                LoP.width = LoP.zoom * 200;
                        }
                        if (LoP.height < (LoP.zoom * 120)) {
                                LoP.height = LoP.zoom * 120;
                        }
                        // mute
                        if (!strncmp( argv[i], mute_string, 5)) {
                                LoP.mute = TRUE;
                        }
                        // track time
                        if (!strncmp( argv[i], track_time_string, 5)) {
                                LoP.track_time = TRUE;
                        }
                        //printf("mute is: %s\n", LoP.mute ? "TRUE" : "FALSE");
                }
        } // end parse command line options
	*/

	//// our surfaces
	//
	// main screen
	SDL_Surface *screen;

	// title screen and temp surface for scaling
	SDL_Surface *title;
	SDL_Surface *title2;

        // intro scrolling text
        SDL_Surface *intro;
        SDL_Surface *intro2;

	// main ground tiles (background)
	SDL_Surface *terrain;
	SDL_Surface *terrain2;

	// trying to initialize this to all NULL to make Windows memory happy. :/
	int terrain_text[8000] = { 0 };
	// fill the array
	get_terrain_text(terrain_text);

	// surface to draw tiles to before final render
	SDL_Surface *sceneSurface;

	// characters (ed. and monsters)
	SDL_Surface *characters;
	SDL_Surface *characters2;

        // bosses
        SDL_Surface *bosses;
        SDL_Surface *bosses2;

	// letters of the alphabet
	SDL_Surface *alphabet;
	SDL_Surface *alphabet2;
        int alphabet_width = 8 * LoP.zoom;
        // our header
        SDL_Surface *header;
        // our death screen 
        SDL_Surface *end_screen;
        // room darkness
        SDL_Surface *room_darkness;
        // end surfaces
          
	// Setup screen
        if (LoP.full) {
                screen = SDL_SetVideoMode(
                                  LoP.width,
				  LoP.height,
				  16,
				  SDL_FULLSCREEN|SDL_HWSURFACE|SDL_DOUBLEBUF);
        } else {
                screen = SDL_SetVideoMode(
                                  LoP.width,
				  LoP.height,
				  16,
				  SDL_HWSURFACE|SDL_DOUBLEBUF);
        }
	if (screen == NULL) {
		complainAndExit();
		return 1;
	} // end Setup screen

	// a rect for keeping the source and dest coords for rendering
	SDL_Rect src, dest;

	// color key color for transparency
	Uint32 colorkey = 0;

	// is our game running?
	short int running = TRUE;       // used for the whole game
        int title_timer = 0;            // timer for title and intro timeout
        int intro_timer = 0;            // controlled scrolling text
        short int title_running = TRUE;         // are we at the title screen?
        short int intro_running = FALSE;
	short int quit = FALSE;

	// SDL event for keyboard input
	SDL_Event event;

	/* initialize SDL's video system */
	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER|SDL_INIT_AUDIO) != 0) {
		complainAndExit();
	}

        // make sure SDL quits cleanly
	atexit(SDL_Quit);

	/* hide the cursor. */
	SDL_ShowCursor(SDL_DISABLE);

	/* Set the window title */
	SDL_WM_SetCaption("The Legend of Pandora", NULL);

	/* Load the bitmap files */
	// title
	#ifdef __POCC__
		title2 = SDL_LoadBMP("content\\title.bmp");
	#endif
	#ifdef __linux__
		title2 = SDL_LoadBMP("content/title.bmp");
	#endif
	if (title2 == NULL) {
		complainAndExit();
		return 1;
	}
	title = SDL_DisplayFormat(title2);
	SDL_FreeSurface(title2);
	// intro 
	#ifdef __POCC__
		intro2 = SDL_LoadBMP("content\\intro.bmp");
	#endif
	#ifdef __linux__
		intro2 = SDL_LoadBMP("content/intro.bmp");
	#endif
	if (intro2 == NULL) {
		complainAndExit();
		return 1;
	}
	intro = SDL_DisplayFormat(intro2);
	//SDL_FreeSurface(intro2);
	// terrain
	#ifdef __POCC__
		terrain2 = SDL_LoadBMP("content\\terrain.bmp");
	#endif
	#ifdef __linux__
		terrain2 = SDL_LoadBMP("content/terrain.bmp");
	#endif
	if (terrain2 == NULL) {
		complainAndExit();
		return 1;
	}
	terrain = SDL_DisplayFormat(terrain2);
	//SDL_FreeSurface(terrain2);
	// characters
	#ifdef __POCC__
		characters2 = SDL_LoadBMP("content\\characters.bmp");
	#endif
	#ifdef __linux__
		characters2 = SDL_LoadBMP("content/characters.bmp");
	#endif
	if (characters2 == NULL) {
		complainAndExit();
		return 1;
	}
	characters = SDL_DisplayFormat(characters2);
	// bosses
	#ifdef __POCC__
		bosses2 = SDL_LoadBMP("content\\bosses.bmp");
	#endif
	#ifdef __linux__
		bosses2 = SDL_LoadBMP("content/bosses.bmp");
	#endif
	if (bosses2 == NULL) {
		complainAndExit();
		return 1;
	}
	bosses = SDL_DisplayFormat(bosses2);
        // alphabet
	#ifdef __POCC__
		alphabet2 = SDL_LoadBMP("content\\alphabet.bmp");
	#endif
	#ifdef __linux__
		alphabet2 = SDL_LoadBMP("content/alphabet.bmp");
	#endif
	if (alphabet2 == NULL) {
		complainAndExit();
		return 1;
	}
	alphabet = SDL_DisplayFormat(alphabet2);
        // header
        header = SDL_DisplayFormat(alphabet2);
        // death screen
        end_screen = SDL_DisplayFormat(screen);
        // room darkness
        room_darkness= SDL_DisplayFormat(screen);
	/* Done loading bitmap files. */

	// set the video colormap
	if (title->format->palette != NULL) {
		SDL_SetColors(screen,
			      title->format->palette->colors,
			      0,
			      title->format->palette->ncolors);
	}
	/* Grow our surface to scale properly with the screen. */
        title2 = grow(title, LoP.zoom);
        intro2 = grow(intro, LoP.zoom);
        terrain2 = grow(terrain, LoP.zoom);
        bosses2 = grow(bosses, LoP.zoom);
        characters2 = grow(characters, LoP.zoom);
        alphabet2 = grow(alphabet, LoP.zoom);
        header = grow(title, LoP.zoom);
        sceneSurface = grow(title, LoP.zoom);   // this looks odd but
        // I can keep the same format by treating sceneSurface
        // like terrain2. I just draw over it later anyway.
	/* end video prep */

	/* audio setup */
        // music
        Mix_Music *intro_theme = NULL;
        Mix_Music *overworld = NULL;
        Mix_Music *underworld = NULL;
        Mix_Music *boss_theme = NULL;
        // sound effects
	Mix_Chunk *ed_swish = NULL; // mixchunk is for mixing sounds
	Mix_Chunk *ed_pain = NULL;
	Mix_Chunk *troll_pain = NULL;
        Mix_Chunk *bat  = NULL;
        Mix_Chunk *rat  = NULL;
        Mix_Chunk *steps_down = NULL;
        Mix_Chunk *steps_up = NULL;
        Mix_Chunk *win = NULL;
        Mix_Chunk *rock = NULL;
        Mix_Chunk *flame = NULL;
        Mix_Chunk *shake = NULL;
        Mix_Chunk *king_troll_pain = NULL;
        Mix_Chunk *vanfire_pain = NULL;
        Mix_Chunk *tempus_pain = NULL;
        Mix_Chunk *tink = NULL;
        Mix_Chunk *waves = NULL;
        Mix_Chunk *life = NULL;
	int ed_swish_channel = -1;
	int ed_pain_channel = -1;
	int troll_pain_channel = -1;
        int bat_channel = -1;
        int rat_channel = -1;
        int steps_down_channel = -1;
        int steps_up_channel = -1;
        int win_channel = -1;
        int rock_channel = -1;
        //int flame_channel = -1;
        int shake_channel = -1;
        int king_troll_pain_channel = -1;
        int vanfire_pain_channel = -1;
        int tempus_pain_channel = -1;
        int tink_channel = -1;
        int waves_channel = -1;
        int life_channel = -1;
        // setup
	int audio_rate = 22050;
	Uint16 audio_format = AUDIO_S16;
	int audio_channels = 2;	// stereo
	int audio_buffers = 4096;
	if (Mix_OpenAudio(audio_rate,
			  audio_format,
			  audio_channels,
			  audio_buffers)) {
		complainAndExit();
	}
	// load the music
	#ifdef __POCC__ // title theme
    	intro_theme = Mix_LoadMUS("content\\music\\title.wav");
	#endif
	#ifdef __linux__
    	intro_theme = Mix_LoadMUS("content/music/title.wav");
	#endif
        if (intro_theme == NULL) complainAndExit();
	#ifdef __POCC__ // overworld theme
        overworld = Mix_LoadMUS("content\\music\\overworld.wav");
	#endif
	#ifdef __linux__
        overworld = Mix_LoadMUS("content/music/overworld.wav");
	#endif
	if (overworld == NULL) complainAndExit();
	#ifdef __POCC__ // underworld theme
        underworld = Mix_LoadMUS("content\\music\\underworld.wav");
	#endif
	#ifdef __linux__
        underworld = Mix_LoadMUS("content/music/underworld.wav");
	#endif
	if (underworld == NULL) complainAndExit();
	#ifdef __POCC__ // boss fight music
        boss_theme = Mix_LoadMUS("content\\music\\boss.wav");
	#endif
	#ifdef __linux__
        boss_theme = Mix_LoadMUS("content/music/boss.wav");
	#endif
	if (boss_theme == NULL) complainAndExit();
        /* load the sound effects */
        // sword 
	#ifdef __POCC__ 
		ed_swish = Mix_LoadWAV("content\\sounds\\ed_swish.wav");
	#endif
	#ifdef __linux__
		ed_swish = Mix_LoadWAV("content/sounds/ed_swish.wav");
	#endif
	if (ed_swish == NULL) complainAndExit();
        // ed pain
	#ifdef __POCC__
		ed_pain = Mix_LoadWAV("content\\sounds\\ed_pain.wav");
	#endif
	#ifdef __linux__
		ed_pain = Mix_LoadWAV("content/sounds/ed_pain.wav");
	#endif
	if (ed_pain == NULL) complainAndExit();
        // troll pain
	#ifdef __POCC__ 
		troll_pain = Mix_LoadWAV("content\\sounds\\troll_pain.wav");
	#endif
	#ifdef __linux__
		troll_pain = Mix_LoadWAV("content/sounds/troll_pain.wav");
	#endif
	if (troll_pain == NULL) complainAndExit();
        // bat scream
	#ifdef __POCC__ 
		bat = Mix_LoadWAV("content\\sounds\\bat.wav");
	#endif
	#ifdef __linux__
		bat = Mix_LoadWAV("content/sounds/bat.wav");
	#endif
	if (bat == NULL) complainAndExit();
        // rat scream
	#ifdef __POCC__ 
		rat = Mix_LoadWAV("content\\sounds\\rat.wav");
	#endif
	#ifdef __linux__
		rat = Mix_LoadWAV("content/sounds/rat.wav");
	#endif
	if (rat == NULL) complainAndExit();
        // going down steps sound
	#ifdef __POCC__ 
		steps_down = Mix_LoadWAV("content\\sounds\\steps_down.wav");
	#endif
	#ifdef __linux__
		steps_down = Mix_LoadWAV("content/sounds/steps_down.wav");
	#endif
	if (steps_down == NULL) complainAndExit();
        // going up steps sound
	#ifdef __POCC__ 
		steps_up = Mix_LoadWAV("content\\sounds\\steps_up.wav");
	#endif
	#ifdef __linux__
		steps_up = Mix_LoadWAV("content/sounds/steps_up.wav");
	#endif
	if (steps_up == NULL) complainAndExit();
        // win sound for boss defeat
	#ifdef __POCC__ 
		win = Mix_LoadWAV("content\\sounds\\win.wav");
	#endif
	#ifdef __linux__
		win = Mix_LoadWAV("content/sounds/win.wav");
	#endif
	if (win == NULL) complainAndExit();
        // rock crash
	#ifdef __POCC__ 
		rock = Mix_LoadWAV("content\\sounds\\rock.wav");
	#endif
	#ifdef __linux__
		rock = Mix_LoadWAV("content/sounds/rock.wav");
	#endif
	if (rock == NULL) complainAndExit();
        // flame
	#ifdef __POCC__ 
		flame = Mix_LoadWAV("content\\sounds\\flame.wav");
	#endif
	#ifdef __linux__
		flame = Mix_LoadWAV("content/sounds/flame.wav");
	#endif
	if (flame == NULL) complainAndExit();
        // screen shake
	#ifdef __POCC__ 
		shake = Mix_LoadWAV("content\\sounds\\shake.wav");
	#endif
	#ifdef __linux__
		shake = Mix_LoadWAV("content/sounds/shake.wav");
	#endif
	if (shake == NULL) complainAndExit();
        // king troll pain
	#ifdef __POCC__
		king_troll_pain = Mix_LoadWAV("content\\sounds\\king_troll_pain.wav");
	#endif
	#ifdef __linux__
		king_troll_pain = Mix_LoadWAV("content/sounds/king_troll_pain.wav");
	#endif
	if (king_troll_pain == NULL) complainAndExit();
        // VanFire pain
	#ifdef __POCC__
		vanfire_pain = Mix_LoadWAV("content\\sounds\\vanfire_pain.wav");
	#endif
	#ifdef __linux__
		vanfire_pain = Mix_LoadWAV("content/sounds/vanfire_pain.wav");
	#endif
	if (vanfire_pain == NULL) complainAndExit();
        // Tempus pain
	#ifdef __POCC__
		tempus_pain = Mix_LoadWAV("content\\sounds\\tempus_pain.wav");
	#endif
	#ifdef __linux__
		tempus_pain = Mix_LoadWAV("content/sounds/tempus_pain.wav");
	#endif
	if (tempus_pain == NULL) complainAndExit();
        // tink
	#ifdef __POCC__
		tink = Mix_LoadWAV("content\\sounds\\tink.wav");
	#endif
	#ifdef __linux__
		tink = Mix_LoadWAV("content/sounds/tink.wav");
	#endif
	if (tink == NULL) complainAndExit();
        // waves
	#ifdef __POCC__
		waves = Mix_LoadWAV("content\\sounds\\waves.wav");
	#endif
	#ifdef __linux__
		waves = Mix_LoadWAV("content/sounds/waves.wav");
	#endif
	if (waves == NULL) complainAndExit();
        // give life
	#ifdef __POCC__
		life = Mix_LoadWAV("content\\sounds\\life.wav");
	#endif
	#ifdef __linux__
		life = Mix_LoadWAV("content/sounds/life.wav");
	#endif
	if (life == NULL) complainAndExit();
        /* end audio setup */

	/* time variables */
	Uint32 time             = 0;
	Uint32 old_time         = 0;
	Uint32 delta_time       = 0;
        Uint8  hours            = 0;
        Uint8  minutes          = 0;
        Uint8  seconds          = 0;
        Uint16 milliseconds      = 0;
	/* end time variables */

	// variables for drawing scenes to the screen
	int x = 0;    // used for drawing scene tiles to the screen
	int y = 0;
	int scene[300];
        char monster_in_scene[35] = {0};       // in this case 0 = FALSE not Ed
        // set which monsters go in which scene
        for (o = 0; o < 36; o++) {
                switch (o) {
                case 0 ... 4:
                case 11 ... 13:
                case 19 ... 20:
                case 27:
                        monster_in_scene[o] = TROLL;
                        break;
                case 7:
                        monster_in_scene[o] = RAT;
                        break;
                case 8 ... 10:
                case 16 ... 18:
                case 24 ... 26:
                        monster_in_scene[o] = GHOST;
                        break;
                case 5 ... 6: 
                case 14 ... 15:
                case 21 ... 23:
				case 28:
						break;
                case 29 ... 31:
                        monster_in_scene[o] = BAT;
                        break;
                default:
                        monster_in_scene[o] = FALSE;
                        break;
                }
        }
	int scene_number = ED_START_SCENE;     // set where ed starts while we're at it. 
	int scene_change = TRUE;

	// collision detection in world
	short int scene_block[30][20] = {FALSE};

        // timer to delay the game while animating ladder descent into dungeon
        int enter_dungeon_timer = 0;
        // timer to delay the game while animating ladder ascent out of dungeon
        int exit_dungeon_timer = 0;
        // delay before exit timer, once the boss dies, or once they are already dead
        int delay_dungeon_exit_timer = 0;

        // state, if the player is in a special state use these timers and conditions
        struct state {
                // used for different timing
                int timer;
                // is the zone TRUE or FALSE
                char active;
        };

        struct state ed_dead;
        ed_dead.timer = 0;
        ed_dead.active = FALSE;
        
        struct state ed_win;
        ed_win.timer = 0;
        ed_win.active = FALSE;
        
        struct state boss_hurt;
        boss_hurt.timer = 0;
        boss_hurt.active = FALSE;

        struct state ed_hurt;
        ed_hurt.timer = 0;
        ed_hurt.active = FALSE;

        struct state monster_hurt[MAXMONSTER];
        for (i = 0; i < MAXMONSTER; i++) {
                monster_hurt[i].timer = 0;
                monster_hurt[i].active = FALSE;
        }

        struct state game_delayed;
        game_delayed.timer = 0;
        game_delayed.active = FALSE;

        struct state at_ocean;
        at_ocean.timer = 0;
        at_ocean.active = FALSE;
        // an animation that draws a life vial filling at boss.x and boss.y
        struct state give_life;
        give_life.timer = 0;
        give_life.active = FALSE;

	/* character stucture */
	struct character {
		int direction;    // 0==north, 1==south, 2==east, 3==west
                int distance_from_ed_x;         // for AI to change direction
                int distance_from_ed_y;         // for AI to change direction
		int velocity_x;    // -1 0 1
		int velocity_y;
                int speed;
		int x;
		int y;
		int keyframe;

		// which row to draw the character from the character sheet.
		short int row;

		// how many steps and which step the character is on. (used for wobble_frame)
		int step;

		// use this to add delta_time to.
		// Then you can move the character after it grows to a certain point.
		int speed_timer;

                // how many milliseconds before we change keyframes. 
                int frame_speed;

		// whether or not the character has collided into something
		// (bool TRUE/FALSE)
		int collision;

		// if the character has been bumped what direction was it from
		int bump_direction;

		// stores the amount of time passed in order to trip at a certain rate
		Uint32	bump_timer;

		// whether or not the character is being rendered on screen.
		int draw;

		// the health of the character, when zero the character is dead.
		int health;

        // has the character been damaged?
        int hit;

		// is the character invincible? Any positive amount of miliseconds
		// is true and dictates how long they will be invincible
		int invincibility_timer;

		// is attack true?
		int attack;

		// integer of how many milliseconds attack will last
		int attack_timer;

                // a timer for render the death animation 
                int death_animation;
	};

	// create our main character ed.
	struct character ed;
	ed.direction		= SOUTH;
	ed.x			= ED_START_X * LoP.zoom;
	ed.y			= ED_START_Y * LoP.zoom;
	ed.velocity_x		= 0;
	ed.velocity_y		= 0;
        ed.speed                = EDSPEED;
	ed.keyframe		= 0;
	ed.row			= 0;
	ed.step			= 0;
	ed.speed_timer		= 0;
        ed.frame_speed           = EDFRAMESPEED;
	ed.bump_direction	= NO_DIRECTION;
	ed.bump_timer		= 0;
	ed.draw			= TRUE;
	ed.health		= ED_HEALTH;
	ed.invincibility_timer	= 0;
	ed.attack		= 0;
        ed.attack_timer         = 0;
        ed.death_animation      = 0;

        // create monsters
	struct character monster[MAXMONSTER];
	for (o=0;o<MAXMONSTER;o++) {
		monster[o].direction		= SOUTH;
                monster[o].distance_from_ed_x   = 0;
                monster[o].distance_from_ed_y   = 0;
		monster[o].x			= 0;
		monster[o].y			= 0;
                monster[o].speed                = 0;
		monster[o].velocity_x		= 0;
		monster[o].velocity_y		= 0;
		monster[o].keyframe		= 0;
		monster[o].row			= 0;
		monster[o].step			= 0;
		monster[o].speed_timer		= 0;
                monster[o].frame_speed           = 0;
		monster[o].bump_direction	= NO_DIRECTION;
		monster[o].bump_timer		= 0;
		monster[o].draw			= FALSE;
		monster[o].health		= 0;
		monster[o].invincibility_timer	= 0;
		monster[o].attack		= 0;
                monster[o].death_animation      = 0;
	}

        // make a structure for our bosses
        struct boss_struct {
                int direction;
		int velocity_x;
		int velocity_y;
		int x;
		int y;
                int speed;
		int keyframe;
                short int row;
                int step;
                int speed_timer;
                int frame_speed;
                int bump_direction;
                int collision;
                char draw;
                int health;
                int hit;
                int attack;
                int attack_timer;
                int attack_frame_time;
                char attack_mode;
		int invincibility_timer;        // amount of time before boss can be hit again
                int death_animation;
                bool craig_dead;
                bool king_troll_dead;
                bool vanfire_dead;
                bool tempus_dead;
        };

        // boss structure
        struct boss_struct boss;
        boss.direction = SOUTH;
        boss.velocity_x = 0;
        boss.velocity_y = 1;
        boss.x = 40 * LoP.zoom;
        boss.y = 20 * LoP.zoom;
        boss.speed = 0;
        boss.keyframe = 0;
        boss.row = 0;
        boss.speed_timer = 0;
        boss.frame_speed = 2000;
        boss.bump_direction = NO_DIRECTION;
        boss.draw = FALSE;
        boss.health = 0;
        boss.hit = 0;
        boss.attack = FALSE;
        boss.attack_timer = 0;
        boss.attack_frame_time = 0;
        boss.attack_mode = 0;
        boss.invincibility_timer = 0;
        boss.death_animation = 0;
        boss.craig_dead = FALSE;
        boss.king_troll_dead = FALSE;
        boss.vanfire_dead = FALSE;
        boss.tempus_dead = FALSE;

        // struct for screen shake
        struct shaker {
                int active;
                int x;
                int x_timer;
                int timer;
        };
        struct shaker screen_shake;
        screen_shake.active = FALSE;
        screen_shake.x = 0;
        screen_shake.x_timer = 0;
        screen_shake.timer = 0;

	// key input must be virtualized since the system is too literal. 
	struct buttonsStruct {
		int up;
		int down;
		int left;
		int right;
		int a;
		int b;
		int y;
		int x;
	};

	struct buttonsStruct key;
	key.up    = FALSE;
	key.down  = FALSE;
	key.left  = FALSE;
	key.right = FALSE;
	key.a     = FALSE;
	key.b     = FALSE;
	key.y     = FALSE;
	key.x     = FALSE;

        // our game's score
        int score = 0;
        int old_score = 1; // used for updating the screen.
        int header_x_pos = 0;

        // vanfire window health
        int vanfire_window_health[4] = {0};

        // tempus flame health
        int tempus_flame_health[4] = {0};
        int tempus_flame_timer = 0;

	// make rand work by providing it with a seed
	srand(SDL_GetTicks());

	/* game loop */
	while (running) {

                // If the title screen should be running go into this loop
                if (title_running) {
                        // draw the title bitmap
                        src.x = 0;
                        src.y = 0;
                        src.w = LoP.zoom * 200;
                        src.h = LoP.zoom * 120;
                        dest.x = 0;
                        dest.y = 0;
                        dest.w = LoP.zoom * 200;
                        dest.h = LoP.zoom * 120;
                        SDL_BlitSurface(title2, &src, screen, &dest);
                        SDL_Flip(screen);
                        // Play intro theme music
                        Mix_PlayMusic(intro_theme, -1);
                        if (LoP.mute) {
                                Mix_HaltMusic();
                        }
                        // reset the timer
                        title_timer = 0;
                }
                // loop for input
                while (title_running) {
                        // time handling
                        SDL_Delay(30);
                        time=SDL_GetTicks();
                        delta_time = time - old_time;
                        title_timer += delta_time;
                        // see if we have timed out
                        if (title_timer > 8000) {
                                title_running = FALSE;
                                intro_running = TRUE;
                        }
                        // Get key input
                        while (SDL_PollEvent(&event)) {
                                switch (event.type) {
                                        case SDL_KEYUP:
                                                switch (event.key.keysym.sym) {
                                                case SDLK_ESCAPE:
                                                        title_running = FALSE;
                                                        intro_running = FALSE;
                                                        quit = TRUE;
                                                        break;
                                                case SDLK_RETURN:
                                                        title_running = FALSE;
                                                        intro_running = FALSE;
                                                        // stop the intro theme
                                                        Mix_HaltMusic();
                                                        // Play overworld theme
                                                        Mix_PlayMusic(overworld, -1);
                                                        if (LoP.mute) {
                                                                Mix_HaltMusic();
                                                        }
                                                        break;
                                                case SDLK_LALT:
                                                        title_running = FALSE;
                                                        intro_running = FALSE;
                                                        // stop the intro theme
                                                        Mix_HaltMusic();
                                                        // Play overworld theme
                                                        Mix_PlayMusic(overworld, -1);
                                                        if (LoP.mute) {
                                                                Mix_HaltMusic();
                                                        }
                                                        break;
                                                case SDLK_m:
                                                        // mute
                                                        if (LoP.mute) {
                                                                Mix_PlayMusic(intro_theme, -1);
                                                                LoP.mute = FALSE;
                                                        } else {
                                                                Mix_HaltMusic();
                                                                LoP.mute = TRUE;
                                                        }
                                                        break;
                                                case SDLK_LMETA:
                                                case SDLK_q:
                                                //case SDL_QUIT:
                                                        title_running = FALSE;
                                                        intro_running = FALSE;
                                                        quit = TRUE;
                                                        break;
                                                }
                                }
                        }
                        // store old time for delta calculation
                        old_time = time;
                }
                // end title screen

                // Intro text
                // If the title sequence should be running go into this loop
                if (intro_running) {
                        // draw the title bitmap
                        src.x = 0;
                        src.y = 0;
                        src.w = intro2->w;
                        src.h = intro2->h;
                        dest.x = 0;
                        dest.y = 0;
                        dest.w = LoP.zoom * 200;
                        dest.h = LoP.zoom * 120;
                        // reset the timer
                        title_timer = 0;
                }
                // loop for input
                while (intro_running) {
                        // time handling
                        SDL_Delay(30);
                        time=SDL_GetTicks();
                        delta_time = time - old_time;
                        title_timer += delta_time;
                        intro_timer += delta_time;
                        // see if we have timed out
                        if (title_timer > 60000) {
                                title_running = TRUE;
                                intro_running = FALSE;
                        }
                        // time based scroll speed that stops at end
                        if (intro_timer > 100 && src.y < (src.h - (LoP.zoom * 80))) {
                                //printf("src.y:%i\n", src.y);
                                src.y += LoP.zoom;
                                intro_timer = 0;
                        }
                        // Get key input
                        while (SDL_PollEvent(&event)) {
                                switch (event.type) {
                                        case SDL_KEYUP:
                                                switch (event.key.keysym.sym) {
                                                case SDLK_ESCAPE:
                                                        title_running = FALSE;
                                                        intro_running = FALSE;
                                                        quit = TRUE;
                                                        break;
                                                case SDLK_RETURN:
                                                        intro_running = FALSE;
                                                        // stop the intro theme
                                                        Mix_HaltMusic();
                                                        // Play overworld theme
                                                        Mix_PlayMusic(overworld, -1);
                                                        if (LoP.mute) {
                                                                Mix_HaltMusic();
                                                        }
                                                        break;
                                                case SDLK_LALT:
                                                        intro_running = FALSE;
                                                        // stop the intro theme
                                                        Mix_HaltMusic();
                                                        // Play overworld theme
                                                        Mix_PlayMusic(overworld, -1);
                                                        if (LoP.mute) {
                                                                Mix_HaltMusic();
                                                        }
                                                        break;
                                                case SDLK_m:
                                                        // mute
                                                        if (LoP.mute) {
                                                                Mix_PlayMusic(intro_theme, -1);
                                                                LoP.mute = FALSE;
                                                        } else {
                                                                Mix_HaltMusic();
                                                                LoP.mute = TRUE;
                                                        }
                                                        break;
                                                case SDLK_LMETA:
                                                case SDLK_q:
                                                //case SDL_QUIT:
                                                        title_running = FALSE;
                                                        intro_running = FALSE;
                                                        quit = TRUE;
                                                        break;
                                                }
                                }
                        }
                        SDL_BlitSurface(intro2, &src, screen, &dest);
                        SDL_Flip(screen);
                        if (LoP.mute) {
                                Mix_HaltMusic();
                        }
                        // store old time for delta calculation
                        old_time = time;
                }
                // end intro screen
                

                // main game loop below

		/*
		 * Get the time so we can do things like vsync and
		 * time-based movement.
		 * if the game ran 8ms then 8ms will be added to this time, if 24 then 8
		 * subtracted.
		 */
		time = SDL_GetTicks();
		/* find the time delta (how long the last loop took to run.) */
		delta_time = time - old_time;
                // do we keep track of time?
                if (LoP.track_time) {
                        // get our milliseconds
                        milliseconds += delta_time;
                        // convert to seconds
                        if (milliseconds > 999) {
                                milliseconds = 0;
                                seconds += 1;
								printf("tick\n");
                        }
                        // conver to minutes
                        if (seconds > 59) {
                                seconds = 0;
                                minutes += 1;
                        }
                        // convert to hours
                        if (minutes > 59) {
                                minutes = 0;
                                hours += 1;
                        }
                        // stop game at high hours
                        if (hours > 99) {
                                running = FALSE;
                                quit = TRUE;
                        }
                }
		/* Get keyboard input. */
		// you will notice I set 2 to some keys. The 2 allows us to
		// "remember" that
		// the key is still down even though another key is active.
		// when we go to move the ed.we will only look for 1 to move. 2 will be
		// ignored until the other key is released and 2-1 to make the
		// key active again.
		if (SDL_PollEvent(&event)) {
			switch (event.key.keysym.sym) {
			/* up */
			case SDLK_UP:
				key.up = 1;
				ed.direction = NORTH;
				if (key.down) {key.down = 2;}
				if (key.left) {key.left = 2;}
				if (key.right) {key.right = 2;}
				break;
			/* down */
			case SDLK_DOWN:
				key.down = 1;
				ed.direction = SOUTH;
				if (key.up) {key.up= 2;}
				if (key.left) {key.left= 2;}
				if (key.right) {key.right = 2;}
				break;
			/* left */
			case SDLK_LEFT:
				key.left = 1;
				ed.direction = WEST;
				if (key.up) {key.up = 2;}
				if (key.down) {key.down = 2;}
				if (key.right) {key.right = 2;}
				break;
			/* right */
			case SDLK_RIGHT:
				key.right = 1;
				ed.direction = EAST;
				if (key.up) {key.up = 2;}
				if (key.down) {key.down = 2;}
				if (key.left) {key.left = 2;}
				break;
                        // had to add the += instead of just making it true since
                        // SDL thinks it presses down again before key-up. :(
			case SDLK_END:
                        case SDLK_PAGEDOWN:
			case SDLK_SPACE:    // adding space for attack for testing
				key.b += TRUE;
				break;
			case SDLK_LCTRL:    // reset
                                // set title to active
                                title_running = TRUE;
                                // reset Ed
                                ed_dead.active = FALSE;
                                ed.health = ED_HEALTH;
                                ed.draw = TRUE;
                                ed.x = ED_START_X * LoP.zoom;
                                ed.y = ED_START_Y * LoP.zoom;
                                ed.direction = SOUTH;
                                ed.attack = FALSE;
                                ed.attack_timer = 0;
                                ed.speed_timer = 0;
                                // reset timers
                                enter_dungeon_timer = 0;
                                exit_dungeon_timer = 0;
                                delay_dungeon_exit_timer = 0;
                                screen_shake.active = FALSE;
                                screen_shake.timer = 0;
                                give_life.timer = 0;
                                game_delayed.active = FALSE;
                                game_delayed.timer = 0;
                                // reset keys
                                key.down = FALSE;
                                key.up = FALSE;
                                key.left = FALSE;
                                key.right = FALSE;
                                key.b = FALSE;
                                scene_number = ED_START_SCENE;
                                scene_change = TRUE;
                                // reset score
                                score = 0;
                                // reset bosses
                                boss.king_troll_dead = FALSE;
                                boss.vanfire_dead = FALSE;
                                boss.tempus_dead = FALSE;
                                boss.craig_dead = FALSE;
                                boss.attack_mode = 0;
                                boss.attack_timer = 0;
                                boss.speed_timer = 0;
                                vanfire_window_health[0] = 4;
                                vanfire_window_health[1] = 4;
                                vanfire_window_health[2] = 4;
                                vanfire_window_health[3] = 4;
                                tempus_flame_health[0] = 1;
                                tempus_flame_health[1] = 1;
                                tempus_flame_health[2] = 1;
                                tempus_flame_health[3] = 1;
                                // reset time
                                milliseconds = 0;
                                seconds = 0;
                                minutes = 0;
                                hours = 0;
                                break;
			case SDLK_ESCAPE:
			case SDLK_q:
                        case SDLK_LMETA:
				quit = TRUE;
				running = FALSE;
				break;
			default:
				break;
			}
			switch (event.type) {
			case SDL_KEYUP:
				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
				case SDLK_q:
                                case SDLK_LMETA:
					quit = TRUE;
					running = FALSE;
					break;
                                case SDLK_RETURN:
                                case SDLK_LALT:
                                        // if dead Enter or LALT will restart the game
                                        if (ed_dead.active) {
                                                // set title to active
                                                title_running = TRUE;
                                                // reset Ed
                                                ed_dead.active = FALSE;
                                                ed.health = ED_HEALTH;
                                                ed.draw = TRUE;
                                                ed.x = ED_START_X * LoP.zoom;
                                                ed.y = ED_START_Y * LoP.zoom;
                                                ed.direction = SOUTH;
                                                scene_number = ED_START_SCENE;
                                                scene_change = TRUE;
                                                // reset score
                                                score = 0;
                                                // reset bosses
                                                boss.king_troll_dead = FALSE;
                                                boss.vanfire_dead = FALSE;
                                                boss.tempus_dead = FALSE;
                                                boss.craig_dead = FALSE;
                                                boss.attack_mode = 0;
                                                vanfire_window_health[0] = 4;
                                                vanfire_window_health[1] = 4;
                                                vanfire_window_health[2] = 4;
                                                vanfire_window_health[3] = 4;
                                                tempus_flame_health[0] = 1;
                                                tempus_flame_health[1] = 1;
                                                tempus_flame_health[2] = 1;
                                                tempus_flame_health[3] = 1;
                                        }
                                        break;
                                case SDLK_m:
                                        // mute
                                        if (LoP.mute) { // if mute is true
                                                LoP.mute = FALSE;       // set FALSE
                                                // play the correct music for the scene
												if (scene_number <= 31) {
													Mix_PlayMusic(overworld, -1);
												}
												if (scene_number >= 33 && scene_number <= 35) {
													Mix_PlayMusic(boss_theme, -1);
												}
                                        } else {
                                                Mix_HaltMusic();
                                                LoP.mute = TRUE;
                                        }
                                        break;
				case SDLK_UP:
					key.up -= 1;
					if (key.down == 2) {
						key.down -=1;
						ed.direction = SOUTH;
					}
					if (key.left == 2) {
						key.left -=1;
						ed.direction = WEST;
					}
					if (key.right == 2) {
						key.right -=1;
						ed.direction = EAST;
					}
					// reset standing keyframe
					ed.keyframe = 0;
					break;
				case SDLK_DOWN:
					key.down -= 1;
					if (key.up == 2) {
						key.up -=1;
						ed.direction = SOUTH;
					}
					if (key.left == 2) {
						key.left -=1;
						ed.direction = WEST;
					}
					if (key.right == 2) {
						key.right -=1;
						ed.direction = EAST;
					}
					// reset standing keyframe
					ed.keyframe = 3;
					break;
				case SDLK_LEFT:
					key.left  -= 1;
					if (key.up == 2) {
						key.up -=1;
						ed.direction = NORTH;
					}
					if (key.down == 2) {
						key.down -=1;
						ed.direction = SOUTH;
					}
					if (key.right == 2) {
						key.right -=1;
						ed.direction = EAST;
					}
					// reset standing keyframe
					ed.keyframe = 6;
					break;
				case SDLK_RIGHT:
					key.right -= 1;
					if (key.up == 2) {
						key.up -=1;
						ed.direction = NORTH;
					}
					if (key.down == 2) {
						key.down -=1;
						ed.direction = SOUTH;
					}
					if (key.left == 2) {
						key.left -=1;
						ed.direction = WEST;
					}
					// reset standing keyframe
					ed.keyframe = 9;
					break;
				case SDLK_END:
				case SDLK_SPACE:
                                case SDLK_PAGEDOWN:
					if (key.b == TRUE) { key.b = FALSE; }
					else { key.b -= 1; }
					if (ed.direction == NORTH) { ed.keyframe = 0; }
					if (ed.direction == SOUTH) { ed.keyframe = 3; }
					if (ed.direction == WEST) { ed.keyframe = 6; }
					if (ed.direction == EAST) { ed.keyframe = 9; }
					break;
				default:
					break;
				}
			}
		}
                /* Is the game being delayed */
                if (game_delayed.timer > 0) {
                        game_delayed.active = TRUE;
                        game_delayed.timer -= delta_time;
                        // release the keys
                        key.up = FALSE;
                        key.down = FALSE;
                        key.left = FALSE;
                        key.right = FALSE;
                        key.b = FALSE;
                        ed.direction = SOUTH;
                        ed.keyframe = 3;
                }
                if (game_delayed.timer <= 0) {
                        game_delayed.timer = 0;
                        game_delayed.active = FALSE;
                }
                // is ed attacking?
                if (ed.attack_timer > 0) {
                        ed.attack = TRUE;
                        ed.attack_timer -= delta_time;
                }
                if (ed.attack_timer <= 0) {
                        ed.attack_timer = 0;
                        ed.attack = FALSE;
                }
		/*
		 * stuff for game goes here vvv
		 *
		 */
		//// Character movement and AI
		// Monster
		// move monster if velocity has a value and it is being drawn
		// on the screen
		for (o = 0; o < MAXMONSTER; o++) {
			// increment monster speed timer
			if (monster[o].draw) {
                                /*
                                printf ("m%i ", o);
                                printf("speed:%i ", monster[o].speed);
                                printf("speed_timer:%i ", monster[o].speed_timer);
                                printf("step:%i\n", monster[o].step);
                                */
                                // monster collision with ed
                                if (monster[o].draw == TRUE && 
                                    monster[o].death_animation == 0 &&
                                    ed.draw == TRUE) {
                                        // check for collison with Ed
                                        if (detect_collision(monster[o].row, 
                                                        &monster[o].x, 
                                                        &monster[o].y, 
                                                        &monster[o].velocity_x, 
                                                        &monster[o].velocity_y, 
                                                        &monster[o].direction,
                                                        &monster[o].bump_direction,
                                                        &monster[o].hit,
                                                        ED,
                                                        ed.x, 
                                                        ed.y,
                                                        NO_DIRECTION,
                                                        LoP.zoom)) {
                                                // this will turn the monster around
                                                // since they already turn right on 
                                                // collision
                                                turn_right(&monster[o].direction, 
                                                            &monster[o].velocity_x,
                                                            &monster[o].velocity_y);
                                                ed.hit = TRUE;
                                        }
                                }
                                // end monster collision with ed
                                // incrememnt the speed timer
                                monster[o].speed_timer += delta_time;
                                // increment monster step timer
                                monster[o].step += delta_time;
                                // figure out which monster we have
                                switch (monster[o].row) {
                                        case 1: //RAT
                                                monster[o].frame_speed   = RATFRAMESPEED;
                                                monster[o].speed         = RATSPEED;
                                                // random collision to make them less predictable
                                                if ((rand() % 100) == 1) {
                                                        // make it move like it collided with something.
                                                        turn_right(&monster[o].direction, 
                                                                  &monster[o].velocity_x,
                                                                  &monster[o].velocity_y);
                                                }
                                                break;
                                        case 2: // TROLL
                                                monster[o].frame_speed   = TROLLFRAMESPEED;
                                                monster[o].speed         = TROLLSPEED;
                                                // random collision to make them less predictable
                                                if ((rand() % 100) == 1) {
                                                        // make it move like it collided with something.
                                                        //printf("Monster collision with random.\n");
                                                        turn_right(&monster[o].direction, 
                                                                  &monster[o].velocity_x,
                                                                  &monster[o].velocity_y);
                                                }
                                                break;
                                        case 3: // BAT
                                                monster[o].frame_speed   = BATFRAMESPEED;
                                                monster[o].speed         = BATSPEED;
                                                // random collision to make them less predictable
                                                if ((rand() % 100) == 1) {
                                                        // make it move like it collided with something.
                                                        //printf("Monster collision with random.\n");
                                                        turn_right(&monster[o].direction, 
                                                                  &monster[o].velocity_x,
                                                                  &monster[o].velocity_y);
                                                }
                                                break;
                                        case 4: // GHOST
                                                monster[o].frame_speed   = GHOSTFRAMESPEED;
                                                monster[o].speed         = GHOSTSPEED;
                                                // random collision to make them less predictable
                                                if ((rand() % 100) == 1) {
                                                        // make it move like it collided with something.
                                                        //printf("Monster collision with random.\n");
                                                        turn_right(&monster[o].direction, 
                                                                  &monster[o].velocity_x,
                                                                  &monster[o].velocity_y);
                                                }
                                                break;
                                        case 5: // ROCK
                                                monster[o].frame_speed  = ROCKFRAMESPEED;
                                                monster[o].speed        = ROCKSPEED;
                                                // random hit to make them pop
                                                if ((rand() % 100) == 1) {
                                                        if (monster[o].bump_direction == NO_DIRECTION) {
                                                                monster[o].hit = TRUE;
                                                        }
                                                }
                                                break;
                                        default:
                                                monster[o].frame_speed = 0; // something bad happened
                                                break;
                                }
                                if (monster[o].step > monster[o].frame_speed) {
                                        monster[o].step = monster[o].step - monster[o].frame_speed;
                                }
                                // only move the monster if the timer has been tripped
                                if (monster[o].speed_timer > monster[o].speed) {
                                        // reset the monster speed timer
                                        while (monster[o].speed_timer > monster[o].speed) {
                                                monster[o].speed_timer = monster[o].speed_timer - monster[o].speed;
                                        }
                                        // check for bump
                                        if (monster[o].bump_direction == SOUTH) {
                                                // see if the monster hits and if so don't move them
                                                // modify the monster so they are in "future space"
                                                monster[o].y += LoP.zoom * BUMP_SPEED;
                                                for (x=0; x < 20; x++) {
                                                        for (y = 0; y < 12; y++) {
                                                                if (scene_block[x][y]) {
                                                                        if (detect_collision(monster[o].row, 
                                                                                        &monster[o].x, 
                                                                                        &monster[o].y,
                                                                                        &monster[o].velocity_x, 
                                                                                        &monster[o].velocity_y, 
                                                                                        &monster[o].direction,
                                                                                        &monster[o].bump_direction,
                                                                                        &monster[o].hit,
                                                                                        WALL,
                                                                                        x * LoP.tilesize, 
                                                                                        y * LoP.tilesize,
                                                                                        NO_DIRECTION,
                                                                                        LoP.zoom)) {
                                                                                //printf("cancelling south bump\n");
                                                                                turn_left(&monster[o].direction,
                                                                                          &monster[o].velocity_x,
                                                                                          &monster[o].velocity_y);
                                                                                monster[o].y -= LoP.zoom * BUMP_SPEED;
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                        if (monster[o].bump_direction == NORTH) {
                                                // see if the monster hits and if so don't move them
                                                // modify the monster so they are in "future space"
                                                //printf ("bd: %i\n", monster[o].bump_direction);
                                                monster[o].y -= LoP.zoom * BUMP_SPEED;
                                                for (x=0; x < 20; x++) {
                                                        for (y = 0; y < 12; y++) {
                                                                if (scene_block[x][y]) {
                                                                        if (detect_collision(monster[o].row, 
                                                                                        &monster[o].x, 
                                                                                        &monster[o].y,
                                                                                        &monster[o].velocity_x, 
                                                                                        &monster[o].velocity_y, 
                                                                                        &monster[o].direction,
                                                                                        &monster[o].bump_direction,
                                                                                        &monster[o].hit,
                                                                                        WALL,
                                                                                        x * LoP.tilesize, 
                                                                                        y * LoP.tilesize,
                                                                                        NO_DIRECTION,
                                                                                        LoP.zoom)) {
                                                                                //printf("cancelling north bump\n");
                                                                                turn_left(&monster[o].direction,
                                                                                          &monster[o].velocity_x,
                                                                                          &monster[o].velocity_y);
                                                                                monster[o].y += LoP.zoom * BUMP_SPEED;
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                        if (monster[o].bump_direction == EAST) {
                                                // see if the monster hits and if so don't move them
                                                // modify the monster so they are in "future space"
                                                //printf ("bd: %i\n", monster[o].bump_direction);
                                                monster[o].x += LoP.zoom * BUMP_SPEED;
                                                for (x=0; x < 20; x++) {
                                                        for (y = 0; y < 12; y++) {
                                                                if (scene_block[x][y]) {
                                                                        if (detect_collision(monster[o].row, 
                                                                                        &monster[o].x, 
                                                                                        &monster[o].y,
                                                                                        &monster[o].velocity_x, 
                                                                                        &monster[o].velocity_y, 
                                                                                        &monster[o].direction,
                                                                                        &monster[o].bump_direction,
                                                                                        &monster[o].hit,
                                                                                        WALL,
                                                                                        x * LoP.tilesize, 
                                                                                        y * LoP.tilesize,
                                                                                        NO_DIRECTION,
                                                                                        LoP.zoom)) {
                                                                                //printf("cancelling east bump\n");
                                                                                turn_left(&monster[o].direction,
                                                                                          &monster[o].velocity_x,
                                                                                          &monster[o].velocity_y);
                                                                                monster[o].x -= LoP.zoom * BUMP_SPEED;
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                        if (monster[o].bump_direction == WEST) {
                                                // see if the monster hits and if so don't move them
                                                // modify the monster so they are in "future space"
                                                //printf ("bd: %i\n", monster[o].bump_direction);
                                                monster[o].x -= LoP.zoom * BUMP_SPEED;
                                                for (x=0; x < 20; x++) {
                                                        for (y = 0; y < 12; y++) {
                                                                if (scene_block[x][y]) {
                                                                        if (detect_collision(monster[o].row, 
                                                                                        &monster[o].x, 
                                                                                        &monster[o].y,
                                                                                        &monster[o].velocity_x, 
                                                                                        &monster[o].velocity_y, 
                                                                                        &monster[o].direction,
                                                                                        &monster[o].bump_direction,
                                                                                        &monster[o].hit,
                                                                                        WALL,
                                                                                        x * LoP.tilesize, 
                                                                                        y * LoP.tilesize,
                                                                                        NO_DIRECTION,
                                                                                        LoP.zoom)) {
                                                                                //printf("cancelling west bump\n");
                                                                                turn_left(&monster[o].direction,
                                                                                          &monster[o].velocity_x,
                                                                                          &monster[o].velocity_y);
                                                                                monster[o].x += LoP.zoom * BUMP_SPEED;
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                        // check monster velocity
                                        /*
                                        printf ("monster[%i].direction: %i\n", o, monster[o].direction);
                                        printf ("monster[%i].velocity_x: %i\n", o, monster[o].velocity_x);
                                        printf ("monster[%i].velocity_y: %i\n", o, monster[o].velocity_y);
                                        */
                                        // end check for bump
                                        // monster movement AI
                                                // set ed distance from monster x and y
                                        // x
                                        if (ed.x > monster[o].x) {
                                                monster[o].distance_from_ed_x = ed.x - monster[o].x;
                                        } else {
                                                monster[o].distance_from_ed_x = monster[o].x - ed.x;
                                        }
                                        // y
                                        if (ed.y > monster[o].y) {
                                                monster[o].distance_from_ed_y = ed.y - monster[o].y;
                                        } else {
                                                monster[o].distance_from_ed_y = monster[o].y - ed.y;
                                        }
                                        // special ghost AI
                                        if (monster[o].velocity_y == 1) { // down
                                                if (monster[o].bump_direction == NO_DIRECTION)
                                                        // if monster is a ghost
                                                        if (monster[o].row == GHOST &&
                                                            monster[o].distance_from_ed_y >
                                                            monster[o].distance_from_ed_x &&
                                                            ed.y < monster[o].y) {
                                                                monster[o].direction = NORTH;
                                                                monster[o].velocity_y = -1;
                                                        }
                                        }
                                        if (monster[o].velocity_y == -1) {
                                                if (monster[o].bump_direction == NO_DIRECTION) {
                                                        // if monster is a ghost
                                                        if (monster[o].row == GHOST &&
                                                            monster[o].distance_from_ed_y >
                                                            monster[o].distance_from_ed_x &&
                                                            ed.y > monster[o].y) {
                                                                monster[o].direction = SOUTH;
                                                                monster[o].velocity_y = 1;
                                                        }
                                                }
                                        }
                                        if (monster[o].velocity_x == 1) {       // east
                                                if (monster[o].bump_direction == NO_DIRECTION) {
                                                        // if monster is a ghost
                                                        // normal ghost movement
                                                        if (monster[o].row == GHOST &&
                                                            monster[o].distance_from_ed_x >
                                                            monster[o].distance_from_ed_y &&
                                                            ed.x < monster[o].x) {
                                                                monster[o].direction = WEST;
                                                                monster[o].velocity_x = -1;
                                                        }
                                                }
                                        }
                                        if (monster[o].velocity_x == -1) {      // west
                                                if (monster[o].bump_direction == NO_DIRECTION) {
                                                        // if monster is a ghost
                                                        // normal ghost movement
                                                        if (monster[o].row == GHOST &&
                                                            monster[o].distance_from_ed_x >
                                                            monster[o].distance_from_ed_y &&
                                                            ed.x > monster[o].x) {
                                                                monster[o].direction = EAST;
                                                                monster[o].velocity_x = 1;
                                                        }
                                                }
                                        }
                                        // end monster movement AI
                                        // set the correct animation frame
                                        if (monster[o].velocity_y == 1) { // down
                                                if (monster[o].row == BAT) { // bat loops instead of wobbles
                                                        monster[o].keyframe = 3 + loop_frame(monster[o].step,
                                                                           monster[o].frame_speed);
                                                } else { // every other monster wobbles
                                                        monster[o].keyframe = 3 + wobble_frame(monster[o].step,
                                                                           monster[o].frame_speed);
                                                }
                                        }
                                        if (monster[o].velocity_y == -1) {
                                                if (monster[o].row == BAT) {
                                                        monster[o].keyframe = 0 + loop_frame(monster[o].step,
                                                                           monster[o].frame_speed);
                                                } else {
                                                        monster[o].keyframe = 0 + wobble_frame(monster[o].step,
                                                                           monster[o].frame_speed);
                                                }
                                        }
                                        if (monster[o].velocity_x == 1) {
                                                if (monster[o].row == BAT) { // bat loops instead of wobbles
                                                        monster[o].keyframe = 9 + loop_frame(monster[o].step,
                                                                           monster[o].frame_speed);
                                                } else {
                                                        monster[o].keyframe = 9 + wobble_frame(monster[o].step,
                                                                           monster[o].frame_speed);
                                                }
                                        }
                                        if (monster[o].velocity_x == -1) {
                                                if (monster[o].row == BAT) {
                                                        monster[o].keyframe = 6 + loop_frame(monster[o].step,
                                                                           monster[o].frame_speed);
                                                } else {
                                                        monster[o].keyframe = 6 + wobble_frame(monster[o].step,
                                                                           monster[o].frame_speed);
                                                }
                                        }
                                        // end set the correct monster frame
                                        // actually move monster
                                        if (monster[o].velocity_x == 1) {
                                                monster[o].x += LoP.zoom;
                                        }
                                        if (monster[o].velocity_x == -1) {
                                                monster[o].x -= LoP.zoom;
                                        }
                                        if (monster[o].velocity_y == 1) {
                                                monster[o].y += LoP.zoom;
                                        }
                                        if (monster[o].velocity_y == -1) {
                                                monster[o].y -= LoP.zoom;
                                        }
                                        // monster collision with world
                                        for (x = 0; x < 20; x++) {
                                                for (y = 0; y < 12; y++) {
                                                        if (scene_block[x][y] && 
                                                            monster[o].draw) {
                                                                detect_collision(monster[o].row, 
                                                                                &monster[o].x, 
                                                                                &monster[o].y, 
                                                                                &monster[o].velocity_x, 
                                                                                &monster[o].velocity_y, 
                                                                                &monster[o].direction,
                                                                                &monster[o].bump_direction,
                                                                                &monster[o].hit,
                                                                                WALL,
                                                                                x * LoP.tilesize, 
                                                                                y * LoP.tilesize,
                                                                                NO_DIRECTION,
                                                                                LoP.zoom);
                                                        }
                                                }
                                        }
                                        // end actually move monster
                                }
                        }
		} // end monster
                // Bosses
                // Boss animation and movement
                // King Troll
                if (scene_number == 33 && 
                    boss.draw && 
                    !ed_dead.active && 
                    !ed_win.active) {
                        /*
                        for (o = 0; o < MAXMONSTER; o++) {
                                printf("Monster[%i].health: %i\n", o, monster[o].health);
                        }
                        */
                        // increment boss timers unless game delayed
                        if (game_delayed.active == FALSE) {
                                boss.speed_timer += delta_time;
                                boss.attack_timer += delta_time;
                                boss.step += delta_time;
                        }
                        // calculate to make sure no rocks are being drawn
                        for (o = 0, i = 0; i < MAXMONSTER; i++) {
                                if (monster[i].draw == TRUE) {
                                        o++;
                                }
                        }
                        //printf("There are %i rocks left.\n", o);
                        // if any are then don't attack
                        if (boss.attack_timer >= 4000 &&
                            o == 0) {
                                boss.attack_timer = 0;
                                boss.attack = TRUE;
                        } 
                        /*
                        printf("boss.velocity_x: %i\n", boss.velocity_x);
                        printf("boss.velocity_y: %i\n", boss.velocity_y);
                        printf("boss.keyframe: %i\n", boss.keyframe);
                        printf("boss.attack_frame_time: %i\n", boss.attack_frame_time);
                        */
                        // check to see if attacking
                        if (boss.attack) {
                                // do this once
                                if (boss.attack_frame_time == 0) {
                                        screen_shake.timer = SCREEN_SHAKE_DURATION;
                                        boss.attack_frame_time = 500;
                                        boss.velocity_x = 0;
                                        boss.velocity_y = 0;
                                }
                                // then do this
                                if (boss.attack_frame_time > 0) {
                                        boss.attack_frame_time -= delta_time;
                                        if (boss.attack_frame_time <= 0) {
                                                boss.attack_frame_time = 0;
                                                boss.attack = FALSE;
                                                if (boss.direction == NORTH) boss.velocity_y = -1;
                                                if (boss.direction == SOUTH) boss.velocity_y =  1;
                                                if (boss.direction == WEST)  boss.velocity_x = -1;
                                                if (boss.direction == EAST)  boss.velocity_x =  1;
                                        }
                                        ed.velocity_x = 0;
                                        ed.velocity_y = 0;
                                }
                        }
                        // random collision to make KT less predictable
                        if ((rand() % 100) == 1) {
                                // make it move like it collided with something.
                                turn_right(&boss.direction, 
                                          &boss.velocity_x,
                                          &boss.velocity_y);
                        }
                        if (boss.step > boss.frame_speed) {
                                boss.step = boss.step - boss.frame_speed;
                        }
                        // only move the boss if the timer has been tripped
                        // and draw is TRUE
                        if (boss.speed_timer > boss.speed && 
                            boss.draw == TRUE) {
                                // reset the bosspeed timer
                                boss.speed_timer = boss.speed_timer - boss.speed;
                                // set the correct animation frame
                                if (boss.velocity_y == 1) {
                                        boss.keyframe = 0 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.y += LoP.zoom;
                                }
                                if (boss.velocity_y == -1) {
                                        boss.keyframe = 0 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.y -= LoP.zoom;
                                }
                                if (boss.velocity_x == 1) {
                                        boss.keyframe = 4 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.x += LoP.zoom;
                                }
                                if (boss.velocity_x == -1) {
                                        boss.keyframe = 4 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.x -= LoP.zoom;
                                }
                        }
                }
                // end King Troll
                // Count VanFire
                if (scene_number == 34 && 
                    boss.draw && 
                    !ed_dead.active && 
                    !ed_win.active) {
                        //printf("VanFire health: %i\n", boss.health);
                        // increment boss timers unless game delayed
                        if (game_delayed.active == FALSE) {
                                boss.speed_timer += delta_time;
                                boss.attack_timer += delta_time;
                                boss.step += delta_time;
                        }
                        // check to see if attacking
                        if (boss.attack) {
                                // do this once
                                if (boss.attack_frame_time == 0) {
                                        screen_shake.timer = SCREEN_SHAKE_DURATION;
                                        boss.attack_frame_time = 500;
                                        boss.velocity_x = 0;
                                        boss.velocity_y = 0;
                                }
                                // then do this
                                if (boss.attack_frame_time > 0) {
                                        boss.attack_frame_time -= delta_time;
                                        if (boss.attack_frame_time <= 0) {
                                                boss.attack_frame_time = 0;
                                                boss.attack = FALSE;
                                                if (boss.direction == NORTH) boss.velocity_y = -1;
                                                if (boss.direction == SOUTH) boss.velocity_y =  1;
                                                if (boss.direction == WEST)  boss.velocity_x = -1;
                                                if (boss.direction == EAST)  boss.velocity_x =  1;
                                        }
                                }
                        }
                        if (boss.step > boss.frame_speed) {
                                boss.step = boss.step - boss.frame_speed;
                        }
                        // only move the boss if the timer has been tripped
                        // and draw is TRUE
                        if (boss.speed_timer > boss.speed && 
                            boss.draw == TRUE) {
                                // reset the bosspeed timer
                                boss.speed_timer = boss.speed_timer - boss.speed;
                                // set the correct animation frame
                                if (boss.velocity_y == 1) {
                                        boss.keyframe = 0 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.y += LoP.zoom;
                                }
                                if (boss.velocity_y == -1) {
                                        boss.keyframe = 0 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.y -= LoP.zoom;
                                }
                                if (boss.velocity_x == 1) {
                                        boss.keyframe = 4 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.x += LoP.zoom;
                                }
                                if (boss.velocity_x == -1) {
                                        boss.keyframe = 4 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.x -= LoP.zoom;
                                }
                        }
                }
                // end Count VanFire
                // Tempus
                if (scene_number == 35 && 
                    boss.draw && 
                    !ed_dead.active && 
                    !ed_win.active) {
                        // increment boss timers unless game delayed
                        if (game_delayed.active == FALSE) {
                                boss.speed_timer += delta_time;
                                boss.attack_timer += delta_time;
                                boss.step += delta_time;
                        }
                        // if any are then don't attack
                        if (boss.attack_timer >= 8000) {
                                boss.attack_timer = 0;
                                boss.attack = TRUE;
                        } 
                        // check to see if attacking
                        if (boss.attack) {
                                // do this once
                                if (boss.attack_frame_time == 0) {
                                        screen_shake.timer = SCREEN_SHAKE_DURATION;
                                        boss.attack_frame_time = 500;
                                        boss.velocity_x = 0;
                                        boss.velocity_y = 0;
                                }
                                // then do this
                                if (boss.attack_frame_time > 0) {
                                        boss.attack_frame_time -= delta_time;
                                        if (boss.attack_frame_time <= 0) {
                                                boss.attack_frame_time = 0;
                                                boss.attack = FALSE;
                                                if (boss.direction == NORTH) boss.velocity_y = -1;
                                                if (boss.direction == SOUTH) boss.velocity_y =  1;
                                                if (boss.direction == WEST)  boss.velocity_x = -1;
                                                if (boss.direction == EAST)  boss.velocity_x =  1;
                                        }
                                        ed.velocity_x = 0;
                                        ed.velocity_y = 0;
                                }
                        }
                        if (boss.step > boss.frame_speed) {
                                boss.step = boss.step - boss.frame_speed;
                        }
                        // only move the boss if the timer has been tripped
                        // and draw is TRUE
                        if (boss.speed_timer > boss.speed && 
                            boss.draw == TRUE) {
                                // reset the bosspeed timer
                                boss.speed_timer = boss.speed_timer - boss.speed;
                                // set the correct animation frame
                                if (boss.velocity_y == 1) {
                                        boss.keyframe = 0 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.y += LoP.zoom;
                                }
                                if (boss.velocity_y == -1) {
                                        boss.keyframe = 0 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.y -= LoP.zoom;
                                }
                                if (boss.velocity_x == 1) {
                                        boss.keyframe = 4 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.x += LoP.zoom;
                                }
                                if (boss.velocity_x == -1) {
                                        boss.keyframe = 4 + wobble_frame(boss.step, boss.frame_speed);
                                        boss.x -= LoP.zoom;
                                }
                        }
                }
                // end Tempus
                // end boss animation and movement
                /* Ed movement */
		// increment the speed_timer
                if (game_delayed.active == FALSE) {
                        ed.speed_timer += delta_time;
                }
		if (ed.speed_timer > ed.speed) {
			// reset the timer
			ed.speed_timer = ed.speed_timer - ed.speed;
                        // ed collision with monsters
                        for (o = 0; o < MAXMONSTER; o++) {
                                if (monster[o].draw == TRUE && monster[o].death_animation == 0) {
                                        if (detect_collision(ED, 
                                                        &ed.x, 
                                                        &ed.y, 
                                                        &ed.velocity_x,
                                                        &ed.velocity_y,
                                                        &ed.direction,
                                                        &ed.bump_direction,
                                                        &ed.hit,
                                                        monster[o].row,
                                                        monster[o].x, 
                                                        monster[o].y,
                                                        NO_DIRECTION,
                                                        LoP.zoom)) {
                                                if (ed.direction == NORTH) {
                                                        key.up = FALSE;
                                                }
                                                if (ed.direction == SOUTH) {
                                                        key.down = FALSE;
                                                }
                                                if (ed.direction == WEST) {
                                                        key.left = FALSE;
                                                }
                                                if (ed.direction == EAST) {
                                                        key.right = FALSE;
                                                }
                                        }
                                }
                        }
                        // had to add an exception that resets the keyframe after an attack
                        if (ed.attack == FALSE && ed.velocity_x == 0 && ed.velocity_y == 0) {
                                if (ed.direction == NORTH) { ed.keyframe = 0; }
                                if (ed.direction == SOUTH) { ed.keyframe = 3; }
                                if (ed.direction == WEST) { ed.keyframe = 6; }
                                if (ed.direction == EAST) { ed.keyframe = 9; }
                        }
                        // if B is not being pressed and there is no collision then animate
                        // the ed.walking
                        if (!key.b) {
                                // increment ed step timer
                                if (game_delayed.active == FALSE) {
                                        ed.step += delta_time;
                                }
                                // if ed.step is greater than the allowed keyframe animation rate
                                // then reset the timer, add leftover miliseconds.
                                if (ed.step > ed.frame_speed) {
                                        ed.step = ed.step - ed.frame_speed;
                                }
                                if (ed.velocity_y == 1) {
                                        ed.keyframe = 3 + wobble_frame(ed.step, ed.frame_speed);
                                        ed.y += LoP.zoom;
                                }
                                if (ed.velocity_y == -1) {
                                        ed.keyframe = 0 + wobble_frame(ed.step, ed.frame_speed);
                                        ed.y -= LoP.zoom;
                                }
                                if (ed.velocity_x == 1) {
                                        ed.keyframe = 9 + wobble_frame(ed.step, ed.frame_speed);
                                        ed.x += LoP.zoom;
                                }
                                if (ed.velocity_x == -1) {
                                        ed.keyframe = 6 + wobble_frame(ed.step, ed.frame_speed);
                                        ed.x -= LoP.zoom;
                                }
                        }
                        // make sure the key.b is FALSE if atack is still true
                        if (ed.attack == TRUE) { key.b = FALSE; }
                        // ed key in
                        // b (attack) is being pressed put
                        if (key.b && ed.draw && screen_shake.active == FALSE) {
                                if (ed.direction == NORTH) {
                                        ed.attack_timer = ATTACK_DELAY;
                                        // now force the button off.
                                        key.b = FALSE;
                                        // play the sound effect
                                        if (!LoP.mute) {
                                                ed_swish_channel = Mix_PlayChannel(-1, ed_swish, 0);
                                        }
                                }
                                if (ed.direction == SOUTH) {
                                        ed.attack_timer = ATTACK_DELAY;
                                        // now force the button off.
                                        key.b = FALSE;
                                        // play the sound effect
                                        if (!LoP.mute) {
                                                ed_swish_channel = Mix_PlayChannel(-1, ed_swish, 0);
                                        }
                                }
                                if (ed.direction == WEST) {
                                        ed.attack_timer = ATTACK_DELAY;
                                        // now force the button off.
                                        key.b = FALSE;
                                        // play the sound effect
                                        if (!LoP.mute) {
                                                ed_swish_channel = Mix_PlayChannel(-1, ed_swish, 0);
                                        }
                                }
                                if (ed.direction == EAST) {
                                        ed.attack_timer = ATTACK_DELAY;
                                        // now force the button off.
                                        key.b = FALSE;
                                        // play the sound effect
                                        if (!LoP.mute) {
                                                ed_swish_channel = Mix_PlayChannel(-1, ed_swish, 0);
                                        }
                                }
                                // debug: print Ed's location
                                //printf("%i,%i\n", ed.x, ed.y);
                        }
                        // see if the ed invincibility timer is active and
                        // if so subtract time from it
                        if (ed.invincibility_timer > 0) {
                                //printf("ed.invincibility_timer: %i\n", ed.invincibility_timer);
                                ed.invincibility_timer -= delta_time;
                                if (ed.invincibility_timer < 0) {
                                        ed.invincibility_timer = 0;
                                }
                        }
                }
                /* End Ed movement */
                /* dungeon entrance triggers! */
                // King Troll
                if (scene_number == 0 && 
                                ed.x >= (90 * LoP.zoom) && 
                                ed.x <= (91 * LoP.zoom) && 
                                ed.y <= (40 * LoP.zoom)) {
                        // trigger stairs animation
                        ed.draw = FALSE;
                        // to stop trigger
                        ed.x = 0;
                        ed.y = 0;
                        // set the time for ladder descent
                        enter_dungeon_timer = LADDER_CLIMB_TIME;
                        Mix_HaltMusic();
                        if (!LoP.mute) {
                                steps_down_channel = Mix_PlayChannel(-1, steps_down, 0);
                        }
                        game_delayed.timer = 2000;
                }
                // Count VanFire
                if (scene_number == 15 && 
                                ed.x >= (100 * LoP.zoom) && 
                                ed.x <= (101 * LoP.zoom) && 
                                ed.y <= (71 * LoP.zoom)) {
                        // trigger stairs animation
                        ed.draw = FALSE;
                        // move ed to stop trigger
                        ed.x = 0;
                        ed.y = 0;
                        // set the time for ladder descent
                        enter_dungeon_timer = LADDER_CLIMB_TIME;
                        Mix_HaltMusic();
                        if (!LoP.mute) {
                                steps_down_channel = Mix_PlayChannel(-1, steps_down, 0);
                        }
                        //game_delayed.timer = 5000;
                }
                // Tempus
                if (scene_number == 25 && 
                                ed.x >= (90 * LoP.zoom) && 
                                ed.x <= (91 * LoP.zoom) && 
                                ed.y <= (61 * LoP.zoom)) {
                        // trigger stairs animation
                        ed.draw = FALSE;
                        // to stop trigger
                        ed.x = 0;
                        ed.y = 0;
                        // set the time for ladder descent
                        enter_dungeon_timer = LADDER_CLIMB_TIME;
                        Mix_HaltMusic();
                        if (!LoP.mute)
                                steps_down_channel = Mix_PlayChannel(-1, steps_down, 0);
                        //game_delayed.timer = 5000;
                }
                // Craig
                if (scene_number == 28 && 
                                ed.x >= (50 * LoP.zoom) && 
                                ed.x <= (51 * LoP.zoom) && 
                                ed.y <= (41 * LoP.zoom)) {
                        // trigger stairs animation
                        ed.draw = FALSE;
                        // to stop trigger
                        ed.x = 0;
                        ed.y = 0;
                        //printf("scene 28 dungeon triggered! ed.draw is %s\n", ed.draw ? "TRUE" : "FALSE");
                        // set the time for ladder descent
                        enter_dungeon_timer = LADDER_CLIMB_TIME;
                        Mix_HaltMusic();
                        if (!LoP.mute)
                                steps_down_channel = Mix_PlayChannel(-1, steps_down, 0);
                        //game_delayed.timer = 5000;
                }
                /* end dungeon entrance triggers! */
                /* Ed screen boundry detection */
                if (ed.draw) {
                        // ed can go one tile shy of the edge
                        if (ed.x > 182 * LoP.zoom) {
                                // the far right tiles, so block the character from walking,
                                // no scene change.
                                if (scene_number == 7 ||
                                    scene_number == 15 ||
                                    scene_number == 23 ||
                                    scene_number == 31) {
                                        ed.x = 181 * LoP.zoom;
                                        key.right = FALSE;
                                } else {
                                        scene_change = TRUE;
                                        scene_number++;
                                        ed.x = 6 * LoP.zoom;
                                }
                        }
                        // the far left tiles
                        if (ed.x < 6 * LoP.zoom) {
                                if (scene_number == 0 ||
                                    scene_number == 8 ||
                                    scene_number == 16 ||
                                    scene_number == 24) {
                                        ed.x = 6 * LoP.zoom;
                                        key.left = FALSE;
                                } else {
                                        scene_change = TRUE;
                                        scene_number--;
                                        ed.x = 181 * LoP.zoom;
                                }
                        }
                        if (ed.y > 101 * LoP.zoom) {
                                // the bottom tiles
                                if (scene_number > 23) {
                                        ed.y = 100 * LoP.zoom;
                                        key.down = FALSE;
                                } else {
                                        scene_change = TRUE;
                                        scene_number += 8;
                                        ed.y = 30 * LoP.zoom;
                                }
                        }
                        if (ed.y < 25 * LoP.zoom) {
                                // the top scenes
                                if (scene_number < 8) {
                                        ed.y = 25 * LoP.zoom;
                                        key.up = FALSE;
                                } else {
                                        scene_change = TRUE;
                                        scene_number -= 8;
                                        ed.y = 100 * LoP.zoom;
                                }
                        }
                        // the far left tiles
                        if (ed.x < 6 * LoP.zoom) {
                                if (scene_number == 0 ||
                                    scene_number == 8 ||
                                    scene_number == 16 ||
                                    scene_number == 24) {
                                        ed.x = 6 * LoP.zoom;
                                        key.left = FALSE;
                                } else {
                                        scene_change = TRUE;
                                        scene_number--;
                                        ed.x = 181 * LoP.zoom;
                                }
                        }
                        if (ed.y > 101 * LoP.zoom) {
                                // the bottom tiles
                                if (scene_number > 23) {
                                        ed.y = 100 * LoP.zoom;
                                        key.down = FALSE;
                                } else {
                                        scene_change = TRUE;
                                        scene_number += 8;
                                        ed.y = 30 * LoP.zoom;
                                }
                        }
                        if (ed.y < 25 * LoP.zoom) {
                                // the top scenes
                                if (scene_number < 8) {
                                        ed.y = 26 * LoP.zoom;
                                        key.up = FALSE;
                                } else {
                                        scene_change = TRUE;
                                        scene_number -= 8;
                                        ed.y = 100 * LoP.zoom;
                                }
                        }
                }
                /* End Ed screen boundry detection */
                if (!scene_change) {
                        // ed collision with world
                        for (x = 0; x < 20; x++) {
                                for (y = 0; y < 12; y++) {
                                        if (scene_block[x][y]) {
                                                // check for collison
                                                detect_collision(ED, 
                                                                &ed.x, 
                                                                &ed.y, 
                                                                &ed.velocity_x,
                                                                &ed.velocity_y,
                                                                &ed.direction,
                                                                &ed.bump_direction,
                                                                &ed.hit,
                                                                WALL,
                                                                x * LoP.tilesize, 
                                                                y * LoP.tilesize,
                                                                NO_DIRECTION,
                                                                LoP.zoom);
                                        }
                                }
                        }
                        // end ed collision with objects
                        // end test for ed collision
                        // test for ed hit
                        if (ed.hit && (ed.invincibility_timer <= 0)) {
                                ed.health -= 1;
                                ed.hit = FALSE;
                                ed.invincibility_timer = ED_INVINCIBILITY_TIME;
                                if (ed.health <= 0) {
                                        ed_dead.active = TRUE;
                                        ed_dead.timer = 2500; // multiplied by 10 to slow the transition
                                        ed.draw = FALSE;
                                        //ed.death_animation = 1000;
                                }
                                if (!LoP.mute) {
                                        ed_pain_channel= Mix_PlayChannel(-1, ed_pain, 0);
                                }
                                ed_hurt.active = TRUE;
                        } 
                        if (ed.hit && ed.invincibility_timer > 0) {
                                ed.hit = FALSE;
                                //printf("ed.invincibility_timer: %i\n", ed.invincibility_timer);
                        }
                        // end test for ed hit
                        // monster collision with screen edges
                        for (o=0; o < MAXMONSTER; o++) { // max boundries
                                if (monster[o].draw == TRUE) {
                                        detect_collision(monster[o].row, 
                                                        &monster[o].x, 
                                                        &monster[o].y, 
                                                        &monster[o].velocity_x, 
                                                        &monster[o].velocity_y, 
                                                        &monster[o].direction,
                                                        &monster[o].bump_direction,
                                                        &monster[o].hit,
                                                        BOUNDRY,
                                                        0, 
                                                        0,
                                                        NO_DIRECTION,
                                                        LoP.zoom);
                                }
                        }
                        // end monster collision with screen edges
                        // check for monster collision with sword
                        for (o = 0; o < MAXMONSTER; o++) {
                                if (ed.attack == TRUE && monster[o].draw == TRUE) {
                                        // if Ed is north
                                        if (ed.direction == NORTH) {
                                                detect_collision(monster[o].row, 
                                                                &monster[o].x, 
                                                                &monster[o].y, 
                                                                &monster[o].velocity_x, 
                                                                &monster[o].velocity_y, 
                                                                &monster[o].direction,
                                                                &monster[o].bump_direction,
                                                                &monster[o].hit,
                                                                SWORD,
                                                                ed.x, 
                                                                ed.y - LoP.tilesize,
                                                                ed.direction,
                                                                LoP.zoom);
                                        }
                                        if (ed.direction == SOUTH) {
                                                detect_collision(monster[o].row, 
                                                                &monster[o].x, 
                                                                &monster[o].y, 
                                                                &monster[o].velocity_x, 
                                                                &monster[o].velocity_y, 
                                                                &monster[o].direction,
                                                                &monster[o].bump_direction,
                                                                &monster[o].hit,
                                                                SWORD, 
                                                                ed.x, 
                                                                ed.y + LoP.tilesize,
                                                                ed.direction,
                                                                LoP.zoom);
                                        }
                                        if (ed.direction == WEST) {
                                                detect_collision(monster[o].row, 
                                                                &monster[o].x, 
                                                                &monster[o].y, 
                                                                &monster[o].velocity_x, 
                                                                &monster[o].velocity_y, 
                                                                &monster[o].direction,
                                                                &monster[o].bump_direction,
                                                                &monster[o].hit,
                                                                SWORD, 
                                                                ed.x - LoP.tilesize, 
                                                                ed.y,
                                                                ed.direction,
                                                                LoP.zoom);
                                        }
                                        if (ed.direction == EAST) {
                                                detect_collision(monster[o].row, 
                                                                &monster[o].x, 
                                                                &monster[o].y, 
                                                                &monster[o].velocity_x, 
                                                                &monster[o].velocity_y, 
                                                                &monster[o].direction,
                                                                &monster[o].bump_direction,
                                                                &monster[o].hit,
                                                                SWORD, 
                                                                ed.x + LoP.tilesize, 
                                                                ed.y,
                                                                ed.direction,
                                                                LoP.zoom);
                                        }
                                }
                                // check monster timer
                                if (monster[o].invincibility_timer > 0) {
                                        monster[o].invincibility_timer -= delta_time;
                                        //printf("monster invuln: %i\n", monster[o].invincibility_timer);
                                        if (monster[o].invincibility_timer < 0) {
                                                monster[o].invincibility_timer = 0;
                                        }
                                }
                                // check for monster hit
                                if (monster[o].hit && (monster[o].invincibility_timer == 0)) {
                                        // clear velocity
                                        monster[o].velocity_x = 0;
                                        monster[o].velocity_y = 0;
                                        monster[o].direction = ed.direction;
                                        monster[o].health -= 1;
                                        monster[o].hit = FALSE;
                                        monster_hurt[o].active = TRUE;
                                        //printf("monster[%i] hit! health is %i\n", o, monster[o].health);
                                        monster[o].invincibility_timer = MONSTER_INVINCIBILITY_TIME;
                                        // is the monster dead?
                                        if (monster[o].health <= 0) {
                                                // cancel monster hurt
                                                monster_hurt[o].active = FALSE;
                                                monster_hurt[o].timer = 0;
                                                // death animation
                                                monster[o].draw = FALSE;
                                                monster[o].death_animation = 1000;
                                                if (monster[o].row != ROCK) {
                                                        score += 1;
                                                }
                                                //  check for ghost 0 for Tempus level
                                                if (scene_number == 35 && o == 0) {
                                                        if (tempus_flame_health[0] > 0) {
                                                                tempus_flame_health[0] -= 1;
                                                                boss_hurt.active = TRUE;
                                                                // play hit sound
                                                                if (!LoP.mute) {
                                                                        tempus_pain_channel = Mix_PlayChannel(-1, tempus_pain, 0);
                                                                }
                                                        } else {
                                                                if (tempus_flame_health[1] > 0) {
                                                                        tempus_flame_health[1] -= 1;
                                                                        boss_hurt.active = TRUE;
                                                                        // play hit sound
                                                                        if (!LoP.mute) {
                                                                                tempus_pain_channel = Mix_PlayChannel(-1, tempus_pain, 0);
                                                                        }
                                                                } else {
                                                                        if (tempus_flame_health[2] > 0) {
                                                                                tempus_flame_health[2] -= 1;
                                                                                boss_hurt.active = TRUE;
                                                                                // play hit sound
                                                                                if (!LoP.mute) {
                                                                                        tempus_pain_channel = Mix_PlayChannel(-1, tempus_pain, 0);
                                                                                }
                                                                        } else {
                                                                                if (tempus_flame_health[3] > 0) {
                                                                                        tempus_flame_health[3] -= 1;
                                                                                        boss_hurt.active = TRUE;
                                                                                        // play hit sound
                                                                                        if (!LoP.mute) {
                                                                                                tempus_pain_channel = Mix_PlayChannel(-1, tempus_pain, 0);
                                                                                        }
                                                                                } 
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                        switch (monster[o].row) {
                                        case RAT:
                                                if (!LoP.mute) {
                                                        rat_channel = Mix_PlayChannel(-1, rat, 0);
                                                }
                                                break;
                                        case TROLL:
                                                if (!LoP.mute) {
                                                        troll_pain_channel = Mix_PlayChannel(-1, troll_pain, 0);
                                                }
                                                break;
                                        case BAT:
                                                if (!LoP.mute) {
                                                        bat_channel = Mix_PlayChannel(-1, bat, 0);
                                                }
                                                break;
                                        case GHOST:
                                                break;
                                        case ROCK:
                                                if (!LoP.mute) {
                                                        rock_channel = Mix_PlayChannel(-1, rock, 0);
                                                }
                                        default:
                                                break;
                                        }
                                }
                                if (monster[o].hit && monster[o].invincibility_timer > 0) {
                                        // clear velocity
                                        monster[o].velocity_x = 0;
                                        monster[o].velocity_y = 0;
                                        monster[o].hit = FALSE;
                                }
                        }
                        // end check for monster collision with weapon
                        // monster test for collision and change direction
                        for (o = 0; o < MAXMONSTER; o++) {
                                if (monster[o].draw &&
                                    monster[o].row != ROCK) {
                                        //printf ("M%i d: %i\n", o, monster[o].direction);
                                }
                        }
                        // end monster test for collision and change direction
                        // increment bump timer and cancel monster bump on timeout
                        for (o = 0; o < MAXMONSTER; o++) {
                                // dead?
                                if (monster[o].draw == FALSE) {
                                        monster[o].bump_timer = 0;
                                        monster[o].bump_direction = NO_DIRECTION;
                                }
                                // bumped?
                                if (monster[o].bump_direction != NO_DIRECTION) {
                                        monster[o].bump_timer += delta_time;
                                        //printf("bump_timer: %i bump_direction: %i\n", monster[o].bump_timer, monster[o].bump_direction);
                                }
                                if (monster[o].bump_timer > BUMP_TIME) {
                                        monster[o].direction = monster[o].bump_direction;
                                        monster[o].bump_timer = 0;
                                        monster[o].bump_direction = NO_DIRECTION;
                                        // set velocity
                                        if (monster[o].direction == NORTH) {
                                                monster[o].velocity_x = 0;
                                                monster[o].velocity_y = -1;
                                        }
                                        if (monster[o].direction == SOUTH) {
                                                monster[o].velocity_x = 0;
                                                monster[o].velocity_y = 1;
                                        }
                                        if (monster[o].direction == WEST) {
                                                monster[o].velocity_x = -1;
                                                monster[o].velocity_y = 0;
                                        }
                                        if (monster[o].direction == EAST) {
                                                monster[o].velocity_x = 1;
                                                monster[o].velocity_y = 0;
                                        }
                                        /*
                                        printf ("monster[%i].direction: %i\n", o, monster[o].direction);
                                        printf ("monster[%i].velocity_x: %i\n", o, monster[o].velocity_x);
                                        printf ("monster[%i].velocity_y: %i\n", o, monster[o].velocity_y);
                                        */
                                }
                        }
                        // end increment bump timer and cancel monster bump on timeout
                        /* Boss collision */
                        // check for boss collision with sword
                        if ((scene_number == 32 || 
                             scene_number == 33 ||
                             scene_number == 34 ||
                             scene_number == 35) &&
                            ed.attack && 
                            boss.health > 0) {
                                if (ed.direction == NORTH) {
                                        detect_collision(boss.row + 10, 
                                                        &boss.x, 
                                                        &boss.y, 
                                                        &boss.velocity_x, 
                                                        &boss.velocity_y, 
                                                        &boss.direction,
                                                        &boss.bump_direction,
                                                        &boss.hit,
                                                        SWORD,
                                                        ed.x, 
                                                        ed.y - LoP.tilesize,
                                                        NO_DIRECTION,
                                                        LoP.zoom);
                                }
                                if (ed.direction == SOUTH) {
                                        detect_collision(boss.row + 10, 
                                                        &boss.x, 
                                                        &boss.y, 
                                                        &boss.velocity_x, 
                                                        &boss.velocity_y, 
                                                        &boss.direction,
                                                        &boss.bump_direction,
                                                        &boss.hit,
                                                        SWORD, 
                                                        ed.x, 
                                                        ed.y + LoP.tilesize,
                                                        NO_DIRECTION,
                                                        LoP.zoom);
                                }
                                if (ed.direction == WEST) {
                                        detect_collision(boss.row + 10, 
                                                        &boss.x, 
                                                        &boss.y, 
                                                        &boss.velocity_x, 
                                                        &boss.velocity_y, 
                                                        &boss.direction,
                                                        &boss.bump_direction,
                                                        &boss.hit,
                                                        SWORD, 
                                                        ed.x - LoP.tilesize, 
                                                        ed.y,
                                                        NO_DIRECTION,
                                                        LoP.zoom);
                                }
                                if (ed.direction == EAST) {
                                        detect_collision(boss.row + 10, 
                                                        &boss.x, 
                                                        &boss.y, 
                                                        &boss.velocity_x, 
                                                        &boss.velocity_y, 
                                                        &boss.direction,
                                                        &boss.bump_direction,
                                                        &boss.hit,
                                                        SWORD, 
                                                        ed.x + LoP.tilesize, 
                                                        ed.y,
                                                        NO_DIRECTION,
                                                        LoP.zoom);
                                }
                                // Craig hit
                                if (boss.hit && 
                                    boss.invincibility_timer == 0 &&
                                    scene_number == 32) {
                                        boss.invincibility_timer = BOSS_INVINCIBILITY_TIME;
                                        boss.hit = FALSE;
                                        boss.health -= 1;
                                        //boss.attack = TRUE;
                                        //boss.attack_mode += 1;
                                        //boss.speed -= boss.attack_mode;
                                        boss_hurt.active = TRUE;
                                        boss.attack_timer = 0;
                                        //printf("boss attack: %i\n", boss.attack_mode);
                                        //printf("boss health: %i\n", boss.health);
                                        //Give player full health
                                        //ed.health = ED_HEALTH;
                                        // play hit sound
                                        if (!LoP.mute) {
                                                king_troll_pain_channel = Mix_PlayChannel(-1, king_troll_pain, 0);
                                        }
                                }
                                // end Craig hit
                                // King Troll hit
                                if (boss.hit && 
                                    boss.invincibility_timer == 0 &&
                                    scene_number == 33) {
                                        boss.invincibility_timer = BOSS_INVINCIBILITY_TIME;
                                        boss.hit = FALSE;
                                        boss.health -= 1;
                                        boss.attack = TRUE;
                                        boss.attack_mode += 1;
                                        boss.speed -= boss.attack_mode;
                                        boss_hurt.active = TRUE;
                                        boss.attack_timer = 0;
                                        //printf("boss attack: %i\n", boss.attack_mode);
                                        //printf("boss health: %i\n", boss.health);
                                        // play hit sound
                                        if (!LoP.mute) {
                                                king_troll_pain_channel = Mix_PlayChannel(-1, king_troll_pain, 0);
                                        }
                                }
                                // end King Troll hit
                                // Count VanFire hit
                                if (scene_number == 34 && !boss.vanfire_dead) {
                                        boss.health = (vanfire_window_health[0] +
                                                       vanfire_window_health[1] +
                                                       vanfire_window_health[2] +
                                                       vanfire_window_health[3]);
                                }
                                if (boss.hit &&
                                    boss.invincibility_timer == 0 &&
                                    scene_number == 34) {
                                        boss.invincibility_timer = BOSS_INVINCIBILITY_TIME;
                                        boss.hit = FALSE;
                                        //printf("CVF hit!\n");
                                        // play tink sound because hitting him does nothing
                                        if (!LoP.mute) {
                                                tink_channel = Mix_PlayChannel(-1, tink, 0);
                                        }
                                }
                                // end VanFire hit
                                // tempus hit
                                if (scene_number == 35 && !boss.tempus_dead) {
                                        boss.health = (tempus_flame_health[0] +
                                                       tempus_flame_health[1] +
                                                       tempus_flame_health[2] +
                                                       tempus_flame_health[3]);
                                }
                                // end tempus hit
                                if (boss.invincibility_timer > 0) {
                                        //printf("boss.invincibility_timer: %i\n",
                                        //boss.invincibility_timer);
                                        boss.invincibility_timer -= delta_time;
                                        if (boss.invincibility_timer < 0) {
                                                boss.invincibility_timer = 0;
                                        }
                                        boss.hit = FALSE;
                                }
                                // if the boss died
                                if (boss.health <= 0) {
                                        boss.death_animation = 1000;
                                        score += 1000;
                                        // set the correct boss to dead
                                        if (scene_number == 32) {
                                                boss.craig_dead = TRUE;
                                        }
                                        if (scene_number == 33) {
                                                boss.king_troll_dead = TRUE;
                                        }
                                        if (scene_number == 34) {
                                                boss.vanfire_dead = TRUE;
                                        }
                                        if (scene_number == 35) {
                                                boss.tempus_dead = TRUE;
                                        }
                                        boss.direction = NO_DIRECTION;
                                        boss.velocity_x = 0;
                                        boss.velocity_y = 0;
                                        boss_hurt.active = FALSE;
                                        boss_hurt.timer = 0;
                                        ed_hurt.active = FALSE;
                                        ed_hurt.timer = 0;
                                }
                        }
                        // check for ed collision with Craig
                        if (scene_number == 32 && boss.health > 0) { // if in Craig's cave
								//printf("Craig collision!\n");
                                detect_collision(ED, 
                                                &ed.x, 
                                                &ed.y, 
                                                &ed.velocity_x,
                                                &ed.velocity_y,
                                                &ed.direction,
                                                &ed.bump_direction,
                                                &ed.hit,
                                                CRAIG, 
                                                boss.x, 
                                                boss.y,
                                                NO_DIRECTION,
                                                LoP.zoom);
                        }
                        // Ed collision with King Troll 
                        if (scene_number == 33 && 
                            !boss.king_troll_dead &&
                            !ed_dead.active) {
                                detect_collision(ED, 
                                                &ed.x, 
                                                &ed.y, 
                                                &ed.velocity_x,
                                                &ed.velocity_y,
                                                &ed.direction,
                                                &ed.bump_direction,
                                                &ed.hit,
                                                KING_TROLL, 
                                                boss.x, 
                                                boss.y,
                                                NO_DIRECTION,
                                                LoP.zoom);
                        }
                        // King Troll collision detection with world
                        if ((scene_number == 33) && (!boss.king_troll_dead)) {
                                for (x=0; x < 20; x++) {
                                        for (y = 0; y < 12; y++) {
                                                if (scene_block[x][y]) {
                                                        detect_collision(KING_TROLL, 
                                                                        &boss.x, 
                                                                        &boss.y, 
                                                                        &boss.velocity_x, 
                                                                        &boss.velocity_y, 
                                                                        &boss.direction,
                                                                        &boss.bump_direction,
                                                                        &boss.hit,
                                                                        WALL,
                                                                        x * LoP.tilesize, 
                                                                        y * LoP.tilesize,
                                                                        NO_DIRECTION,
                                                                        LoP.zoom);
                                                }
                                        }
                                }
                        }
                        // Ed collision with Count Van Fire
                        if (scene_number == 34 && 
                            !boss.vanfire_dead &&
                            !ed_dead.active) {
                                detect_collision(ED, 
                                                &ed.x, 
                                                &ed.y, 
                                                &ed.velocity_x,
                                                &ed.velocity_y,
                                                &ed.direction,
                                                &ed.bump_direction,
                                                &ed.hit,
                                                VANFIRE, 
                                                boss.x, 
                                                boss.y,
                                                NO_DIRECTION,
                                                LoP.zoom);
                        }
                        // Sword collision with windows on scene 34
                        if (scene_number == 34 && 
                            !boss.vanfire_dead &&
                            screen_shake.timer == 0 &&
                            ed.attack_timer == ATTACK_DELAY) {
                                // window 1
                                if (ed.direction == NORTH) {
                                        detect_collision(WINDOW1, 
                                                        &boss.x, 
                                                        &boss.y, 
                                                        &boss.velocity_x, 
                                                        &boss.velocity_y, 
                                                        &boss.direction,
                                                        &boss.bump_direction,
                                                        &boss.hit,
                                                        SWORD,
                                                        ed.x, 
                                                        ed.y - LoP.tilesize - (LoP.zoom * 4),
                                                        NO_DIRECTION,
                                                        LoP.zoom);
                                }
                                if (boss.hit) {
                                        if (vanfire_window_health[0] > 0) {
                                                //printf("w1:%i\n", vanfire_window_health[0]);
                                                boss.attack_timer = 0;
                                                boss.attack = TRUE;
                                                boss_hurt.active = TRUE;
                                        }
                                        vanfire_window_health[0] -= 1;
                                        if (vanfire_window_health[0] < 0) {
                                                vanfire_window_health[0] = 0;
                                        }
                                        boss.hit = FALSE;
                                }
                                // window 2
                                if (ed.direction == NORTH) {
                                        detect_collision(WINDOW2, 
                                                        &boss.x, 
                                                        &boss.y, 
                                                        &boss.velocity_x, 
                                                        &boss.velocity_y, 
                                                        &boss.direction,
                                                        &boss.bump_direction,
                                                        &boss.hit,
                                                        SWORD,
                                                        ed.x, 
                                                        ed.y - LoP.tilesize - (LoP.zoom * 4),
                                                        NO_DIRECTION,
                                                        LoP.zoom);
                                }
                                if (boss.hit) {
                                        if (vanfire_window_health[1] > 0) {
                                                boss.attack_timer = 0;
                                                boss.attack = TRUE;
                                                boss_hurt.active = TRUE;
                                        }
                                        vanfire_window_health[1] -= 1;
                                        if (vanfire_window_health[1] < 0) {
                                                vanfire_window_health[1] = 0;
                                        }
                                        boss.hit = FALSE;
                                }
                                // window 3
                                if (ed.direction == NORTH) {
                                        detect_collision(WINDOW3, 
                                                        &boss.x, 
                                                        &boss.y, 
                                                        &boss.velocity_x, 
                                                        &boss.velocity_y, 
                                                        &boss.direction,
                                                        &boss.bump_direction,
                                                        &boss.hit,
                                                        SWORD,
                                                        ed.x, 
                                                        ed.y - LoP.tilesize - (LoP.zoom * 4),
                                                        NO_DIRECTION,
                                                        LoP.zoom);
                                }
                                if (boss.hit) {
                                        if (vanfire_window_health[2] > 0) {
                                                boss.attack_timer = 0;
                                                boss.attack = TRUE;
                                                boss_hurt.active = TRUE;
                                        }
                                        vanfire_window_health[2] -= 1;
                                        if (vanfire_window_health[2] < 0) {
                                                vanfire_window_health[2] = 0;
                                        }
                                        boss.hit = FALSE;
                                }
                                // window 4
                                if (ed.direction == NORTH) {
                                        detect_collision(WINDOW4, 
                                                        &boss.x, 
                                                        &boss.y, 
                                                        &boss.velocity_x, 
                                                        &boss.velocity_y, 
                                                        &boss.direction,
                                                        &boss.bump_direction,
                                                        &boss.hit,
                                                        SWORD,
                                                        ed.x, 
                                                        ed.y - LoP.tilesize - (LoP.zoom * 4),
                                                        NO_DIRECTION,
                                                        LoP.zoom);
                                }
                                if (boss.hit) {
                                        if (vanfire_window_health[3] > 0) {
                                                boss.attack_timer = 0;
                                                boss.attack = TRUE;
                                                boss_hurt.active = TRUE;
                                        }
                                        vanfire_window_health[3] -= 1;
                                        if (vanfire_window_health[3] < 0) {
                                                vanfire_window_health[3] = 0;
                                        }
                                        boss.hit = FALSE;
                                }
                                if (boss_hurt.active) {
                                        if (!LoP.mute) {
                                                vanfire_pain_channel = Mix_PlayChannel(-1, vanfire_pain, 0);
                                        }
                                }
                        }
                        // VanFire collision detection with world
                        if ((scene_number == 34) && (!boss.vanfire_dead)) {
                                for (x=0; x < 20; x++) {
                                        for (y = 0; y < 12; y++) {
                                                if (scene_block[x][y]) {
                                                        detect_collision(VANFIRE, 
                                                                        &boss.x, 
                                                                        &boss.y, 
                                                                        &boss.velocity_x, 
                                                                        &boss.velocity_y, 
                                                                        &boss.direction,
                                                                        &boss.bump_direction,
                                                                        &boss.hit,
                                                                        WALL,
                                                                        x * LoP.tilesize, 
                                                                        y * LoP.tilesize,
                                                                        NO_DIRECTION,
                                                                        LoP.zoom);
                                                }
                                        }
                                }
                        }
                        // Tempus collision with world
                        if ((scene_number == 35) && (!boss.tempus_dead)) {
                                for (x=0; x < 20; x++) {
                                        for (y = 0; y < 12; y++) {
                                                if (scene_block[x][y]) {
                                                        detect_collision(TEMPUS, 
                                                                        &boss.x, 
                                                                        &boss.y, 
                                                                        &boss.velocity_x, 
                                                                        &boss.velocity_y, 
                                                                        &boss.direction,
                                                                        &boss.bump_direction,
                                                                        &boss.hit,
                                                                        WALL,
                                                                        x * LoP.tilesize, 
                                                                        y * LoP.tilesize,
                                                                        NO_DIRECTION,
                                                                        LoP.zoom);
                                                }
                                        }
                                }
                        }
                }
		// set ed.velocity.
		if (!key.up && !key.down) ed.velocity_y = 0;
		if (!key.left && !key.right) ed.velocity_x = 0;
                if (ed.draw) {
                        if (key.up == 1) {
                                ed.velocity_y = -1;
                                ed.velocity_x = 0;
                        }
                        if (key.down == 1) {
                                ed.velocity_y = 1;
                                ed.velocity_x = 0;
                        }
                        if (key.left == 1) {
                                ed.velocity_x = -1;
                                ed.velocity_y = 0;
                        }
                        if (key.right == 1) {
                                ed.velocity_x = 1;
                                ed.velocity_y = 0;
                        }
                }
		/*
		 * Now we parse our array and draw the scene tiles to the screen.
		 * Remember the screen is 20x10 because of the status bar.
		 */
		if (scene_change) {
			//clear the colision array
			for (y = 0; y < 12; y++) {
				for (x = 0; x < 20; x++) {
					scene_block[x][y] = FALSE;
				}
			}
                        // set if we are at the ocean
                        if (scene_number == 7  ||
                            scene_number == 8  ||
                            scene_number == 15 ||
                            scene_number == 16 ||
                            scene_number == 23 ||
                            scene_number == 24 ||
                            scene_number == 27 ||
                            scene_number == 28 ||
                            scene_number == 29 ||
                            scene_number == 30 ||
                            scene_number == 31) {
                                at_ocean.active = TRUE;
                        } else {
                                at_ocean.active = FALSE;
                                at_ocean.timer = 0;
                        }
			/* get the scene you want to draw. */
			for (i = 0; i < 200; i++) {
				scene[i] = get_map(scene_number, terrain_text, i);
			}
			// a big thanks to TimoVJL from the pellesC forums for helping me out
			// AND
			// getting this section of the code cleaned up.
			src.w = LoP.tilesize;
			src.h = LoP.tilesize;
			dest.w = LoP.zoom * 200;
			dest.h = LoP.zoom * 120;
			for (i = 0, y = 2; y < 12; y++) {
				for (x = 0; x < 20; x++, i++) {
					dest.x = x*LoP.tilesize;
					dest.y = y*LoP.tilesize;
                                        get_tile_coords(scene[i], &src.x, &src.y, &scene_block[x][y], LoP.zoom);
                                        // modify map for VanFire
                                        if (scene_number == 34) {
                                                // first window
                                                scene_block[2][4] = TRUE;
                                                scene_block[3][4] = TRUE;
                                                scene_block[4][4] = TRUE;
                                                scene_block[5][4] = TRUE;
                                                // second window
                                                scene_block[7][4] = TRUE;
                                                scene_block[8][4] = TRUE;
                                                scene_block[9][4] = TRUE;
                                                // third window
                                                scene_block[11][4] = TRUE;
                                                scene_block[12][4] = TRUE;
                                                scene_block[13][4] = TRUE;
                                                // fourth window
                                                scene_block[15][4] = TRUE;
                                                scene_block[16][4] = TRUE;
                                                scene_block[17][4] = TRUE;
                                        }
                                        /* render brown collision boxes
                                        if (scene_block[x][y] == TRUE) {
                                                src.x=80 * LoP.zoom;
                                                src.y=110 * LoP.zoom;
                                        }
                                        */
                                        SDL_BlitSurface(terrain2,
                                                        &src,
                                                        sceneSurface,
                                                        &dest);
                                }
                        }
                        //printf("scene: %i\n", scene_number);
			// reset the monster spawn locations
			for (o = 0; o < MAXMONSTER; o++) {
				monster[o].draw = FALSE;
				monster[o].health = 0;
				monster[o].x = 0;
				monster[o].y = 0;
				monster[o].velocity_x = 0;
				monster[o].velocity_y = 0;
                                monster[o].bump_timer = 0;
                                monster[o].bump_direction = NO_DIRECTION;
                                monster[o].death_animation = 0;
                                monster_hurt[o].active = FALSE;
                                monster_hurt[o].timer = 0;
			}
                        // draw random +3
                        //for (o=(rand() % MAXMONSTER-3) + 3; o > 0; o--) 
                        // draw max
                        for (o=0; o<MAXMONSTER; o++) {
                        // draw one
                        //for (o=0; o < 1; o++) {
                                // rand 2 is either 0 or 1 (TRUE or FALSE)
                                monster[o].velocity_x = 0;
                                monster[o].velocity_y = -1;
                                monster[o].draw = TRUE;
                                // which scene will spawn which monster?
                                monster[o].row = monster_in_scene[scene_number];
                                // set health
                                switch (monster_in_scene[scene_number]) {
                                case RAT:
                                        monster[o].health = RAT_HEALTH;
                                        break;
                                case TROLL:
                                        if (boss.king_troll_dead) {
                                                monster[o].draw = FALSE;
                                                monster[o].health = 0;
                                        } else {
                                                monster[o].health = TROLL_HEALTH;
                                        }
                                        break;
                                case BAT:
                                        if (boss.vanfire_dead) {
                                                monster[o].draw =  FALSE;
                                                monster[o].health = 0;
                                        } else {
                                                monster[o].health = BAT_HEALTH;
                                        }
                                        break;
                                case GHOST:
                                        if (boss.tempus_dead) {
                                                monster[o].draw = FALSE;
                                                monster[o].health = 0;
                                        } else {
                                                monster[o].health = GHOST_HEALTH;
                                        }
                                        break;
                                default:
                                        monster[o].health = 0;
                                        monster[o].draw = FALSE;
                                }
                        }
                        // generate spawn coordinates and separate from the edges of the screen
                        for (o=0;o<MAXMONSTER;o++) {
                                if (monster[o].draw == TRUE) {
                                        // I add the 10 * LoP.zoom to make room for the edges 
                                        // and 30 to Y to make room for the header.
                                        monster[o].x = ((rand() % 170) * LoP.zoom) + (10 * LoP.zoom);
                                        //printf("generated monster[%i] x=%i\n", o, monster[o].x);
                                        monster[o].y = ((rand() % 70) * LoP.zoom) + (30 * LoP.zoom);
                                        //printf("generated monster[%i] y=%i\n", o, monster[o].y);
                                        monster[o].velocity_x = (rand() % 3) - 1; // -1 -> 1
                                        monster[o].velocity_y = (rand() % 3) - 1; // -1 -> 1
                                        //printf("with velocity X:%i Y:%i\n", monster[o].velocity_x, monster[o].velocity_y);
                                        // cover -1,1 and 1,-1 diagonals
                                        if (monster[o].velocity_x + monster[o].velocity_y == 0) monster[o].velocity_x = 0;
                                        //printf("with velocity X:%i Y:%i\n", monster[o].velocity_x, monster[o].velocity_y);
                                        // cover -1,-1 and 1,1 diagonals
                                        if (monster[o].velocity_x + monster[o].velocity_y >= 2 ||
                                           monster[o].velocity_x + monster[o].velocity_y <= -2) monster[o].velocity_x = 0;
                                        //printf("with velocity X:%i Y:%i\n", monster[o].velocity_x, monster[o].velocity_y);
                                        // finally kill the 0,0 possibility
                                        if (monster[o].velocity_x + monster[o].velocity_y == 0) monster[o].velocity_x = 1;
                                        //printf("with velocity X:%i Y:%i\n", monster[o].velocity_x, monster[o].velocity_y);
                                }
                                // cannot spawn on blocked squares
                                for (x=0; x<20; x++) {
                                        for (y=0; y<12; y++) {
                                                if (scene_block[x][y] &&
                                                    monster[o].draw) {
                                                        // tile spawn protection
                                                        if (// top left
                                                            monster[o].x >= (x * LoP.tilesize) &&
                                                            monster[o].x <= (x * LoP.tilesize) + LoP.tilesize &&
                                                            monster[o].y >= (y * LoP.tilesize) &&
                                                            monster[o].y <= (y * LoP.tilesize) + LoP.tilesize ||
                                                            // top right
                                                            monster[o].x + LoP.tilesize >= (x * LoP.tilesize) &&
                                                            monster[o].x + LoP.tilesize <= (x * LoP.tilesize) + LoP.tilesize &&
                                                            monster[o].y >= (y * LoP.tilesize) &&
                                                            monster[o].y <= (y * LoP.tilesize) + LoP.tilesize ||
                                                            // bottom left
                                                            monster[o].x >= (x * LoP.tilesize) &&
                                                            monster[o].x <= (x * LoP.tilesize) + LoP.tilesize &&
                                                            monster[o].y + LoP.tilesize >= (y * LoP.tilesize) &&
                                                            monster[o].y + LoP.tilesize <= (y * LoP.tilesize) + LoP.tilesize ||
                                                            // bottom right
                                                            monster[o].x + LoP.tilesize >= (x * LoP.tilesize) &&
                                                            monster[o].x + LoP.tilesize <= (x * LoP.tilesize) + LoP.tilesize &&
                                                            monster[o].y + LoP.tilesize >= (y * LoP.tilesize) &&
                                                            monster[o].y + LoP.tilesize <= (y * LoP.tilesize) + LoP.tilesize 
                                                            ) {
                                                                /*
                                                                printf("monster[%i].x=%i\tmonster[%i].y=%i\n",
                                                                                o, monster[o].x, o, monster[o].y);
                                                                printf("scene_block[%i][%i]\nleft boundry=%i\n",
                                                                                x, y, (x * LoP.tilesize));
                                                                printf("right boundry=%i\n",
                                                                                (x * LoP.tilesize) + LoP.tilesize);
                                                                printf("top boundry=%i\n",
                                                                                (y * LoP.tilesize));
                                                                printf("bottom boundry=%i\n",
                                                                                (y * LoP.tilesize) + LoP.tilesize);
                                                                */
                                                                monster[o].draw = FALSE;
                                                                //printf("Monster[%i] spawn cancelled!\n", o);
                                                                // draw test square
                                                                /*
                                                                src.x = 100 * LoP.zoom;    // green square
                                                                src.y = 110 * LoP.zoom;
                                                                src.w = LoP.tilesize;
                                                                src.h = LoP.tilesize;
                                                                dest.x  = monster[o].x;
                                                                dest.y  = monster[o].y;
                                                                SDL_BlitSurface(terrain2, &src, sceneSurface, &dest);
                                                                */
                                                        }
                                                }
                                        }
                                }
                        }
                }
                for (o = 0; o < MAXMONSTER && scene_change; o++) {
                        // make sure they can't spawn on Ed. 
                        if (monster[o].draw == TRUE) {
                                //monster[0].x = ed.x + 2;
                                //monster[0].y = ed.y + 2;
                                //printf("Ed coords: %i,%i\n", ed.x, ed.y);
                                //printf("Monster #%i coords: %i,%i\n", o, monster[o].x, monster[o].y);
                                if (monster[o].x > ed.x - (LoP.tilesize * 2) &&
                                   monster[o].x < ed.x + (LoP.tilesize * 3) &&
                                   monster[o].y > ed.y - (LoP.tilesize * 2) &&
                                   monster[o].y < ed.y + (LoP.tilesize * 3)) {
                                        //printf("Monster cannot spawn on Ed!\n");
                                        monster[o].draw = FALSE;
                                        // draw red square to indicate spawn cancel
                                        /*
                                        src.x = 60 * LoP.zoom;
                                        src.y = 110 * LoP.zoom;
                                        src.w = LoP.tilesize;
                                        src.h = LoP.tilesize;
                                        dest.x  = monster[o].x;
                                        dest.y  = monster[o].y;
                                        SDL_BlitSurface(terrain2, &src, sceneSurface, &dest);
                                        */
                                }
                        }
                }
                scene_change = FALSE;
                // test draw the collision map
                /*
                printf("\n-scene----");
                for (y=2;y<12;y++) {
                        printf("\n");
                        for (x=0;x<20;x++) {
                                if (scene_block[x][y]) {
                                        printf("X");
                                }
                                else {
                                        printf("O");
                                }
                        }
                }
                printf("\n----------\n\n");
                */
                /* spawn boss attacks */
                // King Troll and VanFire
                /* screen shake and rocks or bats*/
                // do this once
                if (screen_shake.timer == SCREEN_SHAKE_DURATION) {
                        screen_shake.active = TRUE;
                        screen_shake.x_timer = 100;
                        // spawn rocks
                        if (scene_number == 33) {
                                for (o = 0; o < MAXMONSTER; o++) {
                                        // I add the 10 * LoP.zoom to make room for the edges 
                                        // and 30 to Y to make room for the header.
                                        monster[o].x = ((rand() % 170) * LoP.zoom) + (10 * LoP.zoom);
                                        //printf("generated monster[%i] x=%i\n", o, monster[o].x);
                                        // always spawn at top of screen
                                        monster[o].y = 0 + (30 * LoP.zoom);
                                        monster[o].velocity_x = 0;
                                        monster[o].velocity_y = 1;
                                        monster[o].draw = TRUE;
                                        monster[o].row = 5;
                                        monster[o].health = 1;
                                        monster[o].invincibility_timer = 0;
                                }
                                // play sound
                                if (!LoP.mute) {
                                        shake_channel = Mix_PlayChannel(-1, shake, 0);
                                }
                        }
                        // spawn bats
                        if (scene_number == 34) {
                                for (o = 0; o < MAXMONSTER; o++) {
                                        // always spawn at VanFire's coords
                                        // I add the 10 * LoP.zoom to make room for the edges 
                                        // and 30 to Y to make room for the header.
                                        // Do not overwrite already spawned bats
                                        if (monster[o].draw == FALSE) {
                                                monster[o].x = boss.x;
                                                monster[o].y = boss.y + (rand() % LoP.zoom);
                                        }
                                        // bat direction
                                        if (boss.direction == NORTH) {
                                                monster[o].velocity_x =  0;
                                                monster[o].velocity_y = -1;
                                        }
                                        if (boss.direction == WEST) {
                                                monster[o].velocity_x = -1;
                                                monster[o].velocity_y =  0;
                                        }
                                        if (boss.direction == SOUTH) {
                                                monster[o].velocity_x =  0;
                                                monster[o].velocity_y =  1;
                                        }
                                        if (boss.direction == EAST) {
                                                monster[o].velocity_x =  1;
                                                monster[o].velocity_y =  0;
                                        }
                                        monster[o].draw = TRUE;
                                        monster[o].row = BAT;
                                        monster[o].health = 1;
                                        monster[o].invincibility_timer = 0;
                                        //printf("generated monster[%i] x=%i\n", o, monster[o].x);
                                }
                        }
                        // spawn ghosts
                        if (scene_number == 35) {
                                for (o = 0; o < MAXMONSTER; o++) {
                                        // always spawn at Tempus's coords
                                        // I add the 10 * LoP.zoom to make room for the edges 
                                        // and 30 to Y to make room for the header.
                                        // Do not overwrite already spawned bats
                                        if (monster[o].draw == FALSE) {
                                                monster[o].x = boss.x;
                                                monster[o].y = boss.y + (rand() % LoP.zoom);
                                        }
                                        // ghost direction
                                        if (boss.direction == NORTH) {
                                                monster[o].velocity_x =  0;
                                                monster[o].velocity_y = -1;
                                        }
                                        if (boss.direction == WEST) {
                                                monster[o].velocity_x = -1;
                                                monster[o].velocity_y =  0;
                                        }
                                        if (boss.direction == SOUTH) {
                                                monster[o].velocity_x =  0;
                                                monster[o].velocity_y =  1;
                                        }
                                        if (boss.direction == EAST) {
                                                monster[o].velocity_x =  1;
                                                monster[o].velocity_y =  0;
                                        }
                                        monster[o].draw = TRUE;
                                        monster[o].row = GHOST;
                                        monster[o].health = 2;
                                        monster[o].invincibility_timer = 0;
                                        //printf("generated monster[%i] x=%i\n", o, monster[o].x);
                                }
                        }
                        // play sound
                        if (!LoP.mute) {
                                shake_channel = Mix_PlayChannel(-1, shake, 0);
                        }
                }
                if (screen_shake.timer > 0) {
                        ed.attack = FALSE;
                        screen_shake.timer -= delta_time;
                        screen_shake.x_timer -= delta_time;
						switch (screen_shake.x_timer) {
                        case 0 ... 25:
                                screen_shake.x = LoP.zoom * -1;
                                break;
                        case 26 ... 50:
                                screen_shake.x = 0;
                                break;
                        case 51 ... 75:
                                screen_shake.x = LoP.zoom;
                                break;
                        case 76 ... 100:
                                screen_shake.x = 0;
                                break;
                        case -20 ... -1:
                                if (screen_shake.timer > 0) {
                                        screen_shake.x_timer = 100;
                                }
                                break;
                        default:
                                break;
                        }
                        //printf ("timer: %i\n", screen_shake.timer);
                }
                if (screen_shake.timer <= 0) {
                        screen_shake.timer = 0;
                        screen_shake.active = FALSE;
                }
                /* End screen shake */
                //printf("screen shake duration timer: %i\n", screen_shake.timer);
                // Tempus
                /* end spawn boss attacks */
		// draw sceneSurface to the screen
		src.x = 0;
		src.y = 20 * LoP.zoom;
                src.w = 200 * LoP.zoom;
                src.h = 120 * LoP.zoom;
		dest.x = 0 + screen_shake.x;
		dest.y = 20 * LoP.zoom;
                dest.w = 200 * LoP.zoom;
                dest.h = 120 * LoP.zoom;
		SDL_BlitSurface(sceneSurface, &src, screen, &dest);

		// now lets draw our characters to the screen
                /* render bosses on the screen */
		colorkey = SDL_MapRGB(bosses2->format, 255, 0, 255);
		SDL_SetColorKey(bosses2,
				SDL_SRCCOLORKEY | SDL_RLEACCEL,
				(Uint16) SDL_MapRGB(bosses2->format, 255, 0, 255));
                // Craig
                if (scene_number == 32 && boss.draw && boss.death_animation <= 0) {
                        //printf ("render boss.x: %i\n", boss.x);
                        //printf ("render boss.y: %i\n", boss.y);
                        boss.attack_timer += delta_time;
                        //draw
                        src.w = LoP.tilesize * 2;
                        src.h = LoP.tilesize * 2;
                        src.y = 0;
                        if (boss.attack_timer < 1500) {
                                src.x = 160 * LoP.zoom;
                        } else {
                                src.x = 180 * LoP.zoom;
                        }
                        // reset timer
                        if (boss.attack_timer > 3000) {
                                boss.attack_timer = 0;
                        }
                        dest.x = boss.x;
                        dest.y = boss.y;
                        SDL_BlitSurface(bosses2, &src, screen, &dest);
                }
                // King Troll
                if (scene_number == 33 && 
                    boss.draw && 
                    boss.death_animation <= 0) {
                        // draw
                        if (boss.direction == NORTH) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 0;
                        }
                        if (boss.direction == NORTH && boss.attack) {
                                src.x = 60 * LoP.zoom;
                                src.y = 0;
                        }
                        if (boss.direction == WEST) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 0;
                        }
                        if (boss.direction == WEST && boss.attack) {
                                src.x = 140 * LoP.zoom;
                                src.y = 0;
                        }
                        if (boss.direction == SOUTH) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 20 * LoP.zoom;
                        }
                        if (boss.direction == SOUTH && boss.attack) {
                                src.x = 60 * LoP.zoom;
                                src.y = 20 * LoP.zoom;
                        }
                        if (boss.direction == EAST) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = (TILESIZE * 2) * LoP.zoom;
                        }
                        if (boss.direction == EAST && boss.attack) {
                                src.x = 140 * LoP.zoom;
                                src.y = (TILESIZE * 2) * LoP.zoom;
                        }
                        src.w = LoP.tilesize * 2;
                        src.h = LoP.tilesize * 2;
                        dest.x = (boss.x + screen_shake.x);
                        dest.y = boss.y;
                        //printf ("boss.keyframe: %i\n", boss.keyframe);
                        //printf ("boss.x: %i boss.y: %i\n", boss.x, boss.y);
                        //printf ("boss src.x: %i\nboss src.y: %i\n", src.x, src.y);
                        SDL_BlitSurface(bosses2, &src, screen, &dest);
                }
                // Count VanFire
                if (scene_number == 34) {
                        // render windows 
                        for (i = 0; i < 4; i++) {
                                if (vanfire_window_health[i] == 2) {
                                        src.x = 0;
                                }
                                if (vanfire_window_health[i] == 1) {
                                        src.x = 10 * LoP.zoom;
                                }
                                if (vanfire_window_health[i] == 0) {
                                        src.x = 20 * LoP.zoom;
                                }
                                src.y = 80 * LoP.zoom;
                                src.w = LoP.tilesize;
                                src.h = LoP.tilesize * 2;
                                dest.x = (((i * 40) + 40) * LoP.zoom) + screen_shake.x;
                                dest.y = 40 * LoP.zoom;
                                SDL_BlitSurface(characters2,&src, screen, &dest);
                        }
                        //printf("boss.health: %i\n", boss.health);
                }
                if (scene_number == 34 && 
                    boss.draw && 
                    boss.death_animation <= 0) {
                        // draw
                        if (boss.direction == NORTH) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 40 * LoP.zoom;
                        }
                        if (boss.direction == NORTH && boss.attack) {
                                src.x = 60 * LoP.zoom;
                                src.y = 40 * LoP.zoom;
                        }
                        if (boss.direction == WEST) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 40 * LoP.zoom;
                        }
                        if (boss.direction == WEST && boss.attack) {
                                src.x = 140 * LoP.zoom;
                                src.y = 40 * LoP.zoom;
                        }
                        if (boss.direction == SOUTH) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 60 * LoP.zoom;
                        }
                        if (boss.direction == SOUTH && boss.attack) {
                                src.x = 60 * LoP.zoom;
                                src.y = 60 * LoP.zoom;
                        }
                        if (boss.direction == EAST) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 60 * LoP.zoom;
                        }
                        if (boss.direction == EAST && boss.attack) {
                                src.x = 140 * LoP.zoom;
                                src.y = 60 * LoP.zoom;
                        }
                        src.w = LoP.tilesize * 2;
                        src.h = LoP.tilesize * 2;
                        dest.x = (boss.x + screen_shake.x);
                        dest.y = boss.y;
                        SDL_BlitSurface(bosses2, &src, screen, &dest);
                }
                // Tempus
                if (scene_number == 35 && !boss.tempus_dead) {
                        // check to see if any ghosts exist
                        for (o = 0, i = 0; i < MAXMONSTER; i++) {
                               if (monster[i].draw == TRUE) {
                                       o++;
                               }
                        } // if o > 0 then you still have ghosts rendering
                        // render flames
                        tempus_flame_timer += delta_time;
                        if (tempus_flame_timer > 400) {
                                tempus_flame_timer = 0;
                        }
                        for (i = 0; i < 4; i++) {       // flame flicker
                                switch (tempus_flame_timer) {
                                case 0 ... 99:
                                        src.x = 0;
                                        break;
                                case 100 ... 199:
                                case 300 ... 400:
                                        src.x = 10 * LoP.zoom;
                                        break;
                                case 200 ... 299:
                                        src.x = 20 * LoP.zoom;
                                        break;
                                default:
                                        break;
                                }
                                src.y = 60 * LoP.zoom;
                                src.w = LoP.tilesize;
                                src.h = LoP.tilesize;
                                // render flames at their windows unless ghosts are present
                                if (i == 0) {
                                        dest.x = (10 * LoP.zoom) + screen_shake.x;
                                        dest.y = (40 * LoP.zoom);
                                }
                                if (i == 1) {
                                        dest.x = (40 * LoP.zoom) + screen_shake.x;
                                        dest.y = (40 * LoP.zoom);
                                }
                                if (i == 2) {
                                        dest.x = (140 * LoP.zoom) + screen_shake.x;
                                        dest.y = (40 * LoP.zoom);
                                }
                                if (i == 3) {
                                        dest.x = (170 * LoP.zoom) + screen_shake.x;
                                        dest.y = (40 * LoP.zoom);
                                }
                                // Have flame follow ghost if they are active
                                if (o > 0 && 
                                    i == 0 && 
                                    monster[0].draw &&
                                    tempus_flame_health[0] > 0) {
                                        dest.x = monster[0].x + LoP.tilesize;
                                        dest.y = monster[0].y - LoP.tilesize;
                                }
                                if (o > 0 && 
                                    i != 0 && 
                                    monster[0].draw &&
                                    tempus_flame_health[i] > 0 &&
                                    tempus_flame_health[i - 1] == 0) {
                                        dest.x = monster[0].x + LoP.tilesize;
                                        dest.y = monster[0].y - LoP.tilesize;
                                }
                                //printf("tempus_flame_health[%i]: %i\n", i, tempus_flame_health[i]);
                                // draw if health not zero
                                if (tempus_flame_health[i] > 0) {
                                        SDL_BlitSurface(characters2,&src, screen, &dest);
                                }
                        }
                }
                if (scene_number == 35 && 
                    boss.draw && 
                    boss.death_animation <= 0) {
                        // draw
                        if (boss.direction == NORTH) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 80 * LoP.zoom;
                        }
                        if (boss.direction == NORTH && boss.attack) {
                                src.x = 60 * LoP.zoom;
                                src.y = 80 * LoP.zoom;
                        }
                        if (boss.direction == WEST) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 80 * LoP.zoom;
                        }
                        if (boss.direction == WEST && boss.attack) {
                                src.x = 140 * LoP.zoom;
                                src.y = 80 * LoP.zoom;
                        }
                        if (boss.direction == SOUTH) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 100 * LoP.zoom;
                        }
                        if (boss.direction == SOUTH && boss.attack) {
                                src.x = 60 * LoP.zoom;
                                src.y = 100 * LoP.zoom;
                        }
                        if (boss.direction == EAST) {
                                src.x = (boss.keyframe * (LoP.tilesize * 2));        
                                src.y = 100 * LoP.zoom;
                        }
                        if (boss.direction == EAST && boss.attack) {
                                src.x = 140 * LoP.zoom;
                                src.y = 100 * LoP.zoom;
                        }
                        src.w = LoP.tilesize * 2;
                        src.h = LoP.tilesize * 2;
                        dest.x = (boss.x + screen_shake.x);
                        dest.y = boss.y;
                        SDL_BlitSurface(bosses2, &src, screen, &dest);
                }
                // boss death animation
                if (boss.draw && boss.death_animation > 0) {
                        // do this once
                        if (boss.death_animation == 1000) {
                                Mix_HaltMusic();
                                if (!LoP.mute) {
                                        win_channel = Mix_PlayChannel(-1, win, 0);
                                }
                                ed.velocity_x = 0;
                                ed.velocity_y = 0;
                                // delay game input
                                game_delayed.timer = 2000;
                                // make sure boss is still
                                boss.attack = 0;
                                boss.attack_timer = 0;
                                boss.velocity_x = 0;
                                boss.velocity_y = 0;
                                // kill all monsters
                                for (o = 0; o < MAXMONSTER; o++) {
                                        monster[o].draw = FALSE;
                                }
                        }
                        // draw the smoke
                        switch (boss.death_animation) {
                        case 901 ... 1000:
                                src.x = 160 * LoP.zoom;    // where the death animation is located (beginning)
                                src.y = 20 * LoP.zoom;
                                break;
                        case 751 ... 900:
                                src.x = 160 * LoP.zoom;
                                src.y = 40 * LoP.zoom;
                                break;
                        case 601 ... 750:
                                src.x = 160 * LoP.zoom;
                                src.y = 60 * LoP.zoom;
                                break;
                        case 451 ... 600:
                                src.x = 160 * LoP.zoom;    
                                src.y = 80 * LoP.zoom;
                                break;
                        case 301 ... 450:
                                src.x = 180 * LoP.zoom;    
                                src.y = 20 * LoP.zoom;
                                break;
                        case 151 ... 300:
                                src.x = 180 * LoP.zoom;    
                                src.y = 40 * LoP.zoom;
                                break;
                        case 0 ... 150:
                                src.x = 180 * LoP.zoom;    
                                src.y = 60 * LoP.zoom;
                                break;
                        default:
                                break;
                        }
                        // draw
                        src.w = LoP.tilesize * 2;
                        src.h = LoP.tilesize * 2;
                        dest.x  = boss.x;
                        dest.y  = boss.y;
                        SDL_BlitSurface(bosses2, &src, screen, &dest);
                        boss.death_animation -= delta_time;
                        if (boss.death_animation <= 0) {
                                boss.death_animation = 0;
                                boss.draw = 0;
                                // if craig's scene then give full health
                                if (scene_number == 32) {
                                        give_life.timer = 2000;
                                }
                        }
                }
                // end boss death animation
                // craig giving life animation
                if (give_life.timer > 0) {
                        // do this once
                        if (give_life.timer == 2000) {
                                Mix_HaltMusic();
                                ed.velocity_x = 0;
                                ed.velocity_y = 0;
                                // delay game input
                                game_delayed.timer = 2000;
                                give_life.active = TRUE;
                                // use the boss attack timer for playing sounds
                                boss.attack_timer = 0;
                        }
                        // then do this
                        // play sound if "attack timer" hits 400ms
                        if (boss.attack_timer > 400) {
                                if (!LoP.mute) {
                                        life_channel = Mix_PlayChannel(-1, life, 0);
                                }
                                boss.attack_timer = 0;
                        }
                        // draw the health
                        switch (give_life.timer) {
                        case 1600 ... 2000:
                                src.x = 40 * LoP.zoom;    // where the life timer is 
                                src.y = 30 * LoP.zoom;
                                break;
                        case 1200 ... 1599:
                                src.x = 30 * LoP.zoom;
                                src.y = 30 * LoP.zoom;
                                break;
                        case 800 ... 1199:
                                src.x = 20 * LoP.zoom;
                                src.y = 30 * LoP.zoom;
                                break;
                        case 400 ... 799:
                                src.x = 10 * LoP.zoom;    
                                src.y = 30 * LoP.zoom;
                                break;
                        case 0 ... 399:
                                src.x = 0;    
                                src.y = 30 * LoP.zoom;
                                break;
                        default:
                                break;
                        }
                        // draw
                        src.w = LoP.tilesize;
                        src.h = LoP.tilesize;
                        dest.x  = boss.x + LoP.tilesize;
                        dest.y  = boss.y + LoP.tilesize;
                        SDL_BlitSurface(alphabet2, &src, screen, &dest);
                        give_life.timer -= delta_time;
                        boss.attack_timer += delta_time;
                        //printf("give_life.timer: %i\n", give_life.timer);
                        if (give_life.timer <= 0) {
                                give_life.timer = 0;
                                give_life.active = FALSE;
                                //Give player full health
                                ed.health = ED_HEALTH;
                        }
                }
                // boss hurt square
                if (boss_hurt.active) {
                        boss_hurt.timer += delta_time;
                        // draw red square
                        src.x = 160 * LoP.zoom;
                        src.y = 100 * LoP.zoom;
                        src.w = LoP.tilesize * 2;
                        src.h = LoP.tilesize * 2;
                        dest.x  = boss.x;
                        dest.y  = boss.y;
                        dest.w = src.w;
                        dest.h = src.h;
                        // make it flash
                        switch (boss_hurt.timer) {
                        case 0 ... 100:
                                SDL_BlitSurface(bosses2, &src, screen, &dest);
                                break;
                        case 101 ... 200:
                                //SDL_BlitSurface(bosses2, &src, screen, &dest);
                                break;
                        case 201 ... 300:
                                SDL_BlitSurface(bosses2, &src, screen, &dest);
                                break;
                        case 301 ... 400:
                                //SDL_BlitSurface(bosses2, &src, screen, &dest);
                                break;
                        case 401 ... 500:
                                SDL_BlitSurface(bosses2, &src, screen, &dest);
                                break;
                        case 501 ... 600:
                                //SDL_BlitSurface(bosses2, &src, screen, &dest);
                                break;
                        default:
                                break;
                        }
                        if (boss_hurt.timer > 600) {
                                boss_hurt.timer = 0;
                                boss_hurt.active = FALSE;
                        }
                }
                /* End render bosses on the screen */
		/*
		 * The characters are stored on a magenta background. We
		 * can use the SDL_MapRGB function to obtain the
		 * correct pixel value for pure magenta.
                 */
		colorkey = SDL_MapRGB(characters2->format, 255, 0, 255);
		/* We now enable this surface's colorkey and draw
		 * it again. To turn off the colorkey again, we would
		 * replace the SDL_SRCCOLORKEY flag with zero.
                 */
		SDL_SetColorKey(characters2,
				SDL_SRCCOLORKEY | SDL_RLEACCEL,
				(Uint16) SDL_MapRGB(characters2->format, 255, 0, 255));
		// render ed on screen
                if (ed.draw) {
                        src.x = ed.keyframe * LoP.tilesize;
                        src.y = ed.row * LoP.tilesize;
                        src.w = LoP.tilesize;
                        src.h = LoP.tilesize;
                        dest.x  = ed.x + screen_shake.x;
                        dest.y  = ed.y;
                        SDL_BlitSurface(characters2, &src, screen, &dest);
                }
                // render ed going down the ladder if we are entering a dungeon
                if (enter_dungeon_timer > 0) {
                        src.y = 0;
                        src.w = LoP.tilesize;
                        src.h = LoP.tilesize;
                        // where to draw the ladder to based on scene
                        switch (scene_number) {
                        case 0:         // King Troll
                                dest.x = 90 * LoP.zoom;
                                dest.y = 40 * LoP.zoom;
                                break;
                        case 15:       // Count VanFire
                                dest.x = 100 * LoP.zoom;
                                dest.y = 70 * LoP.zoom;
                                break;
                        case 25:       // Tempus
                                dest.x = 90 * LoP.zoom;
                                dest.y = 60 * LoP.zoom;
                                break;
                        case 28:       // Craig
                                dest.x = 50 * LoP.zoom;
                                dest.y = 40 * LoP.zoom;
                                break;
                        default:
                                break;
                        }
                        switch (enter_dungeon_timer) {
                        case 1 ... LADDER_CLIMB_TIME / 4:
                                //printf("last \t%i\n", enter_dungeon_timer);
                                src.x = 190 * LoP.zoom;
                                SDL_BlitSurface(characters2, &src, screen, &dest);
                                break;
                        case (LADDER_CLIMB_TIME / 4) + 1 ... (LADDER_CLIMB_TIME / 4) * 2:
                                //printf("middle \t%i\n", enter_dungeon_timer);
                                src.x = 180 * LoP.zoom;
                                SDL_BlitSurface(characters2, &src, screen, &dest);
                                break;
                        case ((LADDER_CLIMB_TIME / 4) * 2) + 1 ... (LADDER_CLIMB_TIME / 4) * 3:
                                //printf("first \t%i\n", enter_dungeon_timer);
                                src.x = 170 * LoP.zoom;
                                SDL_BlitSurface(characters2, &src, screen, &dest);
                                break;
                        case ((LADDER_CLIMB_TIME / 4) * 3) + 1 ... LADDER_CLIMB_TIME:
                                //printf("first \t%i\n", enter_dungeon_timer);
                                src.x = 160 * LoP.zoom;
                                SDL_BlitSurface(characters2, &src, screen, &dest);
                                break;
                        default:
                                break;
                        }
                        enter_dungeon_timer -= delta_time;
                        //printf("enter_dungeon_timer: %i\n", enter_dungeon_timer);
                        if (enter_dungeon_timer <= 0) {
                                enter_dungeon_timer = 0;
                                ed.draw = TRUE;
                                scene_change = TRUE;
                                key.up = FALSE;
                                key.down = FALSE;
                                key.left = FALSE;
                                key.right = FALSE;
                                key.b = FALSE;
                                // set the correct scene
                                switch (scene_number) {
                                        // King Troll
                                case 0:
                                        scene_number = 33;
                                        // place ed in the correct spot for the dungeon
                                        ed.x = 20 * LoP.zoom;
                                        ed.y = 40 * LoP.zoom;
                                        ed.direction = SOUTH;
                                        Mix_HaltMusic();
                                        if (!boss.king_troll_dead) {
                                                boss.draw = TRUE;
                                                boss.direction = WEST;
                                                boss.velocity_x = -1;
                                                boss.velocity_y = 0;
                                                boss.frame_speed = KINGTROLLFRAMESPEED;
                                                boss.speed = KINGTROLLSPEED;
                                                boss.attack_mode= 0;
                                                boss.health = 8;
                                                boss.row = 0;
                                                boss.x = 90 * LoP.zoom;
                                                boss.y = 55 * LoP.zoom;
                                                // play boss theme
                                                if (!LoP.mute) {
                                                        Mix_PlayMusic(boss_theme, -1);
                                                }
                                        }
                                        break;
                                // count vanfire
                                case 15: 
                                        // set scene
                                        scene_number = 34;
                                        // place Ed
                                        ed.x = 20 * LoP.zoom;
                                        ed.y = 50 * LoP.zoom;
                                        ed.direction = SOUTH;
                                        Mix_HaltMusic();
                                        if (!boss.vanfire_dead) {
                                                boss.draw = TRUE;
                                                boss.direction = WEST;
                                                boss.velocity_x = -1;
                                                boss.velocity_y = 0;
                                                boss.frame_speed = VANFIREFRAMESPEED;
                                                boss.speed = VANFIRESPEED;
                                                boss.attack_mode= 0;
                                                boss.health = 12;
                                                boss.row = 1;
                                                boss.x = 150 * LoP.zoom;
                                                boss.y = 55 * LoP.zoom;
                                                // set window health
                                                vanfire_window_health[0] = 2;
                                                vanfire_window_health[1] = 2;
                                                vanfire_window_health[2] = 2;
                                                vanfire_window_health[3] = 2;
                                                // play boss theme
                                                if (!LoP.mute) {
                                                        Mix_PlayMusic(boss_theme, -1);
                                                }
                                        }
                                        break;
                                case 25: // tempus
                                        scene_number = 35;
                                        // place Ed
                                        ed.x = 15 * LoP.zoom;
                                        ed.y = 80 * LoP.zoom;
                                        ed.direction = SOUTH;
                                        Mix_HaltMusic();
                                        if (!boss.tempus_dead) {
                                                boss.draw = TRUE;
                                                boss.direction = WEST;
                                                boss.velocity_x = -1;
                                                boss.velocity_y = 0;
                                                boss.frame_speed = TEMPUSFRAMESPEED;
                                                boss.speed = TEMPUSSPEED;
                                                boss.attack_mode= 0;
                                                boss.health = 4;
                                                boss.row = 2;
                                                boss.x = 80 * LoP.zoom;
                                                boss.y = 30 * LoP.zoom;
                                                // set flame health
                                                tempus_flame_health[0] = 1;
                                                tempus_flame_health[1] = 1;
                                                tempus_flame_health[2] = 1;
                                                tempus_flame_health[3] = 1;
                                                // play boss theme
                                                if (!LoP.mute) {
                                                        Mix_PlayMusic(boss_theme, -1);
                                                }
                                        }
                                        break;
                                case 28: // craig
                                        scene_number = 32;
                                        // place ed in the correct spot for the dungeon
                                        ed.x = 10 * LoP.zoom;
                                        ed.y = 30 * LoP.zoom;
                                        ed.direction = SOUTH;
                                        Mix_HaltMusic();
                                        if (!boss.craig_dead) {
                                                boss.draw = TRUE;
                                                boss.health = 1;
                                                boss.row = 3;   //CRAIG
                                                boss.x = 85 * LoP.zoom;
                                                boss.y = 55 * LoP.zoom;
                                        }
                                        break;
                                default:
                                        break;
                                }
                        }
                }
                // end render ed going down the ladder 
                // render ed going up the ladder if we are exiting a dungeon
                if (exit_dungeon_timer > 0) {
                        // reset screen shake
                        screen_shake.active = FALSE;
                        screen_shake.x = 0;
                        screen_shake.x_timer = 0;
                        screen_shake.timer = 0;
                        // set the correct scene
                        if (exit_dungeon_timer == LADDER_CLIMB_TIME) {
                                scene_change = TRUE;
                                switch (scene_number) {
                                case 33: // king troll
                                        scene_number = 0;
                                        ed.draw = FALSE;
                                        ed.x = 90 * LoP.zoom;
                                        ed.y = 45 * LoP.zoom;
                                        Mix_HaltMusic();
                                        break;
                                case 34: // count VanFire
                                        scene_number = 15;
                                        ed.draw = FALSE;
                                        ed.x = 100 * LoP.zoom;
                                        ed.y = 75 * LoP.zoom;
                                        Mix_HaltMusic();
                                        break;
                                case 35: // tempus
                                        scene_number = 25;
                                        ed.draw = FALSE;
                                        ed.x = 90 * LoP.zoom;
                                        ed.y = 65 * LoP.zoom;
                                        Mix_HaltMusic();
                                        break;
                                case 32: // craig
                                        scene_number = 28;
                                        ed.draw = FALSE;
                                        ed.x = 50 * LoP.zoom;
                                        ed.y = 45 * LoP.zoom;
                                        Mix_HaltMusic();
                                        break;
                                default:
                                        break;
                                }
                        }
                        // deincrament the timer
                        exit_dungeon_timer -= delta_time;
                        // where to draw the ladder to based on scene
                        switch (scene_number) {
                        case 0:         // King Troll
                                dest.x = 90 * LoP.zoom;
                                dest.y = 40 * LoP.zoom;
                                break;
                        case 15:       // Count VanFire
                                dest.x = 100 * LoP.zoom;
                                dest.y = 70 * LoP.zoom;
                                break;
                        case 25:       // Tempus
                                dest.x = 90 * LoP.zoom;
                                dest.y = 60 * LoP.zoom;
                                break;
                        case 28:       // Craig
                                dest.x = 50 * LoP.zoom;
                                dest.y = 40 * LoP.zoom;
                                break;
                        default:
                                dest.x = 0;
                                dest.y = 0;
                                break;
                        }
                        src.y = 0;
                        src.w = LoP.tilesize;
                        src.h = LoP.tilesize;
                        switch (exit_dungeon_timer) {
                        case 1 ... (LADDER_CLIMB_TIME / 4):
                                //printf("case 1:\ttimer: %i\n", exit_dungeon_timer);
                                src.x = 160 * LoP.zoom;
                                break;
                        case (LADDER_CLIMB_TIME / 4) + 1 ... (LADDER_CLIMB_TIME / 4) * 2:
                                //printf("case 2,\ttimer: %i\n", exit_dungeon_timer);
                                src.x = 170 * LoP.zoom;
                                break;
                        case ((LADDER_CLIMB_TIME / 4) * 2) + 1 ... (LADDER_CLIMB_TIME / 4) * 3:
                                //printf("case 3,\t: timer%i\n", exit_dungeon_timer);
                                src.x = 180 * LoP.zoom;
                                break;
                        case ((LADDER_CLIMB_TIME / 4) * 3) + 1 ... LADDER_CLIMB_TIME:
                                //printf("case 4,\ttimer: %i\n", exit_dungeon_timer);
                                src.x = 190 * LoP.zoom;
                                break;
                        default:
                                //printf("default,\ttimer: %i\n", exit_dungeon_timer);
                                src.x = 160 * LoP.zoom;
                                break;
                        }
                        //printf ("[%i, %i] [%i, %i]\n", src.x, src.y, dest.x, dest.y);
                        SDL_BlitSurface(characters2, &src, screen, &dest);
                        // if the timer has run out then reset Ed
                        if (exit_dungeon_timer <= 0) {
                                exit_dungeon_timer = 0;
                                ed.draw = TRUE;
                                //scene_change = TRUE;
                                key.up = FALSE;
                                key.down = FALSE;
                                key.left = FALSE;
                                key.right = FALSE;
                                key.b = FALSE;
                                // play overworld theme
                                if (!LoP.mute) {
                                        Mix_PlayMusic(overworld, -1);
                                }
                        }
                }
                // end render ed going up the ladder if we are exiting a dungeon
                // ed hurt square
                if (ed_hurt.active) {
                        ed_hurt.timer += delta_time;
                        // draw red square
                        src.x = 0 * LoP.zoom;
                        src.y = 100 * LoP.zoom;
                        src.w = LoP.tilesize;
                        src.h = LoP.tilesize;
                        dest.x  = ed.x;
                        dest.y  = ed.y;
                        dest.w = src.w;
                        dest.h = src.h;
                        // make it flash
                        switch (ed_hurt.timer) {
                        case 0 ... 100:
                                SDL_BlitSurface(characters2, &src, screen, &dest);
                                break;
                        case 101 ... 200:
                                break;
                        case 201 ... 300:
                                SDL_BlitSurface(characters2, &src, screen, &dest);
                                break;
                        case 301 ... 400:
                                break;
                        case 401 ... 500:
                                SDL_BlitSurface(characters2, &src, screen, &dest);
                                break;
                        case 501 ... 600:
                                break;
                        default:
                                break;
                        }
                        if (ed_hurt.timer > 600) {
                                ed_hurt.timer = 0;
                                ed_hurt.active = FALSE;
                        }
                }
                // end ed hurt square
                // check for dead boss and force exit if so
                if ((boss.craig_dead && scene_number == 32) ||
                    (boss.king_troll_dead && scene_number == 33) ||
                    (boss.vanfire_dead && scene_number == 34) ||
                    (boss.tempus_dead && scene_number == 35)) {
                        if (!ed_win.active) {
                                delay_dungeon_exit_timer += delta_time;
                        } else {
                                delay_dungeon_exit_timer = 0;
                        }
                        //printf ("exit_timer: %i\n", delay_dungeon_exit_timer);
                        //printf ("ed_win.active: %s\n", ed_win.active ? "TRUE" : "FALSE");
                        if (delay_dungeon_exit_timer >= 4000) {
                                // initiate dungeon exit
                                exit_dungeon_timer = LADDER_CLIMB_TIME;
                                // reset delay for dungeon exit
                                delay_dungeon_exit_timer = 0;
                                // play sound of going up stairs
                                if (!LoP.mute) {
                                        steps_up_channel = Mix_PlayChannel(-1, steps_up, 0);
                                }
                        }
                }
		/* render monsters on screen */
                // find out if any monster should be drawn
                for (i = 0, o = 0; i < MAXMONSTER; i++) {
                        //printf("MAXMONSTER is %i\n", MAXMONSTER);
                        //printf("monster[%i].draw is %s\n", i, monster[i].draw ? "TRUE" : "FALSE");
                        if (monster[i].draw || monster[i].death_animation) o++;
                }
                //printf("\n\n\n\n\n\n\n\n\n\nThere are %i monsters alive\n", o);
		if (o > 0) {
			for (o = 0;o<MAXMONSTER;o++) {
				if (monster[o].draw == TRUE) {
                                        //printf("draw is true!\n\nmx:%i my:%i\t, ", monster[o].x, monster[o].y);
					src.x = monster[o].keyframe * LoP.tilesize;
					src.y = monster[o].row * LoP.tilesize;
					src.w = LoP.tilesize;
					src.h = LoP.tilesize;
					dest.x  = monster[o].x;
					dest.y  = monster[o].y;
					SDL_BlitSurface(characters2, &src, screen, &dest);
				}
                                if (monster[o].death_animation) {
                                        switch (monster[o].death_animation) {
                                        case 901 ... 1000:
                                                src.x = 120 * LoP.zoom;    // where the death animation is located (beginning)
                                                break;
                                        case 751 ... 900:
                                                src.x = 130 * LoP.zoom;
                                                break;
                                        case 601 ... 750:
                                                src.x = 140 * LoP.zoom;
                                                break;
                                        case 451 ... 600:
                                                src.x = 150 * LoP.zoom;    
                                                break;
                                        case 301 ... 450:
                                                src.x = 160 * LoP.zoom;    
                                                break;
                                        case 151 ... 300:
                                                src.x = 170 * LoP.zoom;    
                                                break;
                                        case 0 ... 150:
                                                src.x = 180 * LoP.zoom;    
                                                break;
                                        default:
                                                break;
                                        }
                                        if (monster[o].row == ROCK) {
                                                src.y = RAT * LoP.tilesize;
                                        } else {
                                                src.y = monster[o].row * LoP.tilesize;
                                        }
					src.w = LoP.tilesize;
					src.h = LoP.tilesize;
					dest.x  = monster[o].x;
					dest.y  = monster[o].y;
					SDL_BlitSurface(characters2, &src, screen, &dest);
                                        monster[o].death_animation -= delta_time;
                                        //printf("monster[%i].death_animation: %i\n", o, monster[o].death_animation);
                                        if (monster[o].death_animation < 0) monster[o].death_animation = 0;
                                }
			}
                } else {
			for (o=0; o < MAXMONSTER; o++) {
                                // kill all boss monsters
				monster[o].draw = FALSE;
				monster[o].x = 0;
				monster[o].y = 0;
				monster[o].velocity_x = 0;
				monster[o].velocity_y = 0;
                                monster[o].bump_timer = 0;
                                monster[o].bump_direction = NO_DIRECTION;
			}
		}
                // monster hurt square
                for (o=0; o < MAXMONSTER; o++) {
                        if (monster_hurt[o].active) {
                                monster_hurt[o].timer += delta_time;
                                // draw red square
                                src.x = 0 * LoP.zoom;
                                src.y = 100 * LoP.zoom;
                                src.w = LoP.tilesize;
                                src.h = LoP.tilesize;
                                dest.x  = monster[o].x;
                                dest.y  = monster[o].y;
                                dest.w = src.w;
                                dest.h = src.h;
                                // make it flash
                                switch (monster_hurt[o].timer) {
                                case 0 ... 100:
                                        SDL_BlitSurface(characters2, &src, screen, &dest);
                                        break;
                                case 101 ... 200:
                                        break;
                                case 201 ... 300:
                                        SDL_BlitSurface(characters2, &src, screen, &dest);
                                        break;
                                case 301 ... 400:
                                        break;
                                case 401 ... 500:
                                        SDL_BlitSurface(characters2, &src, screen, &dest);
                                        break;
                                case 501 ... 600:
                                        break;
                                default:
                                        break;
                                }
                                if (monster_hurt[o].timer > 600) {
                                        monster_hurt[o].timer = 0;
                                        monster_hurt[o].active = FALSE;
                                }
                        }
                }
                // end monster hurt square
		// render the sword if attacking
		if (ed.attack) {
                        ed.velocity_x = 0;
                        ed.velocity_y = 0;
                        src.w = 10 * LoP.zoom;
                        src.h = 10 * LoP.zoom;
			if (ed.direction == NORTH) {
				ed.keyframe = 12;
				src.x = ed.keyframe * LoP.tilesize;
				src.y = LoP.tilesize * 5;
				dest.x = ed.x + screen_shake.x;
				// character is fighting facing up so subtract LoP.tilesize
				// (the size of a tile) to render above the player
				dest.y = ed.y - LoP.tilesize;
			}
			if (ed.direction == SOUTH) {
				// make sure we subtract the amount of milliseconds the
				// last loop lasted.
				ed.keyframe = 13;
				// we conveniently placed the sword animations under the
				// attack keyframes
				src.x = ed.keyframe * LoP.tilesize;
				src.y = LoP.tilesize * 5;
				dest.x = ed.x + screen_shake.x;
				dest.y = ed.y + LoP.tilesize;
			}
			if (ed.direction == WEST) {
				ed.keyframe = 14;
				src.x = ed.keyframe * LoP.tilesize;
				src.y = LoP.tilesize * 5;       // five is how many tiles deep the sword is
				// now on the x axis. the ed.is attacking west so subtract
				// one tile from x
				dest.x = (ed.x - LoP.tilesize) + screen_shake.x;
				dest.y = ed.y;
			}
			if (ed.direction == EAST) {
				ed.keyframe = 15;
				src.x = ed.keyframe * LoP.tilesize;
				src.y = LoP.tilesize * 5;
				dest.x = (ed.x + LoP.tilesize) + screen_shake.x;
				dest.y = ed.y;
			}
                        // draw
                        SDL_BlitSurface(characters2, &src, screen, &dest);
		}
                /* death screen */
                if (ed_dead.active) {
                        // reset screen shake
                        screen_shake.active = FALSE;
                        screen_shake.x = 0;
                        screen_shake.x_timer = 0;
                        screen_shake.timer = 0;
                        // do these once
                        if (ed_dead.timer == 2500) {
                                Mix_HaltMusic();
                                Mix_PlayMusic(underworld, -1);
                                if (LoP.mute) {
                                        Mix_HaltMusic();
                                }
                                // kill all monsters
                                for (o = 0; o < MAXMONSTER; o++) {
                                        monster[o].draw = FALSE;
                                }
                                // stop wave sound effect
                                at_ocean.active = FALSE;
                        }
                        // do this every time
                        if (ed_dead.timer > 0) {
                                ed_dead.timer -= delta_time;
                                if (ed_dead.timer <= 0) {
                                        ed_dead.timer = 0;
                                }
                        }
                        //printf("ed_dead.timer: %i\n", ed_dead.timer);
                        //printf("ed.health: %i\n", ed.health);
                        // set magenta as transparent
                        colorkey = SDL_MapRGB(alphabet2->format, 255, 0, 255);
                        SDL_SetColorKey(alphabet2,
                                        SDL_SRCCOLORKEY | SDL_RLEACCEL,
                                        (Uint16) SDL_MapRGB(alphabet2->format, 255, 0, 255));
                        // Filling the surface with red color and fading to black
                        SDL_FillRect(end_screen, NULL, SDL_MapRGB(end_screen->format, ed_dead.timer / 10, 0, 0));
                        // write text
                        if (ed_dead.timer == 0) {
                                // shared settings
                                src.w = 8 * LoP.zoom;
                                src.h = 8 * LoP.zoom;
                                dest.w = 8 * LoP.zoom;
                                dest.h = 8 * LoP.zoom;
                                dest.y  = 52 * LoP.zoom;
                                // G 
                                src.x = 40 * LoP.zoom;
                                src.y = 16 * LoP.zoom;
                                dest.x  = 56 * LoP.zoom;        // 32 * 7 * zoom
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // A
                                src.x = 192 * LoP.zoom;
                                src.y = 8 * LoP.zoom;
                                dest.x  = 64 * LoP.zoom;        // 32 * 8 * zoom
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // M
                                src.x = 88 * LoP.zoom;
                                src.y = 16 * LoP.zoom;
                                dest.x  = 72 * LoP.zoom;        // 32 * 9 * zoom
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // E
                                src.x = 24 * LoP.zoom;
                                src.y = 16 * LoP.zoom;
                                dest.x  = 80 * LoP.zoom;        // 32 * 10 * zoom
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // SPACE
                                // O
                                src.x = 104 * LoP.zoom;
                                src.y = 16 * LoP.zoom;
                                dest.x  = 96 * LoP.zoom;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // V
                                src.x = 160 * LoP.zoom;
                                src.y = 16 * LoP.zoom;
                                dest.x  = 104 * LoP.zoom;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // E
                                src.x = 24 * LoP.zoom;
                                src.y = 16 * LoP.zoom;
                                dest.x  = 112 * LoP.zoom;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // R
                                src.x = 128 * LoP.zoom;
                                src.y = 16 * LoP.zoom;
                                dest.x  = 120 * LoP.zoom;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // !
                                src.x = 0;
                                src.y = 0;
                                dest.x  = 128 * LoP.zoom;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                        }
                        // write death screen to screen
                        src.x = 0;
                        src.y = 0;
                        src.w = 200 * LoP.zoom;
                        src.h = 120 * LoP.zoom;
                        dest.x  = 0;
                        dest.y  = 0;
                        dest.w = 200 * LoP.zoom;
                        dest.h = 120 * LoP.zoom;
                        SDL_BlitSurface(end_screen, &src, screen, &dest);
                }                 
                /* end death screen */
                // win screen
                if (ed_win.active == FALSE &&
                    boss.king_troll_dead && 
                    boss.vanfire_dead && 
                    boss.tempus_dead) {
                        ed_win.active = TRUE;
                        ed_win.timer = 2550;
                }
                if (ed_win.active) {
                        // do this once
                        if (ed_win.timer == 2550) {
                                if (!LoP.mute) {
                                        Mix_HaltMusic();
                                        Mix_PlayMusic(intro_theme, -1);
                                }
                                game_delayed.timer = 3000;
                                ed.draw = FALSE;
                                ed.health = 12;
                                at_ocean.active = FALSE;
                                boss.draw = FALSE;
                                boss_hurt.active = FALSE;
                        }
                        // then do this
                        if (ed_win.timer > 0) {
                                ed_win.timer -= delta_time;
                                if (ed_win.timer < 0) ed_win.timer = 0;
                        }
                        // set magenta as transparent
                        colorkey = SDL_MapRGB(alphabet2->format, 255, 0, 255);
                        SDL_SetColorKey(alphabet2,
                                        SDL_SRCCOLORKEY | SDL_RLEACCEL,
                                        (Uint16) SDL_MapRGB(alphabet2->format, 255, 0, 255));
                        // Filling the surface with white color and fading to black
                        SDL_FillRect(end_screen, NULL, SDL_MapRGB(end_screen->format, ed_win.timer / 10, ed_win.timer / 10, ed_win.timer / 10));
                        // write text
                        //printf("ed_win.timer: %i\n", ed_win.timer / 10);
                                dest.w = 8 * LoP.zoom;
                                dest.h = 8 * LoP.zoom;
                                src.y = 16 * LoP.zoom;
                                dest.y  = 42 * LoP.zoom;
                        if (ed_win.timer == 0) {
                                // Y 
                                src.x = 184 * LoP.zoom;
                                src.w = dest.w;
                                src.h = dest.h;
                                dest.x  = dest.w * 8;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // O
                                src.x = 104 * LoP.zoom;
                                dest.x  = dest.w * 9;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // U
                                src.x = 152 * LoP.zoom;
                                dest.x  = dest.w * 10;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // W
                                src.x = 168 * LoP.zoom;
                                dest.x  = dest.w * 12;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // I
                                src.x = 56 * LoP.zoom;
                                dest.x  = dest.w * 13;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // N
                                src.x = 96 * LoP.zoom;
                                dest.x  = dest.w * 14;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                                // !
                                src.x = 0;
                                src.y = 0;
                                dest.x  = dest.w * 15;
                                SDL_BlitSurface(alphabet2, &src, end_screen, &dest);
                        }
                        // write win screen to screen
                        src.x = 0;
                        src.y = 0;
                        src.w = LoP.zoom * 200;
                        src.h = LoP.zoom * 120;
                        dest.x  = 0;
                        dest.y  = 0;
                        dest.w = LoP.zoom * 200;
                        dest.h = LoP.zoom * 120;
                        SDL_BlitSurface(end_screen, &src, screen, &dest);
                } // end win screen
                // render room darkness for VanFire
                if (scene_number == 34 && !ed_dead.active) {
                        // Filling the surface with black
                        SDL_FillRect(room_darkness, NULL, SDL_MapRGB(end_screen->format, 0, 0, 0));
                        // set alpha
                        SDL_SetAlpha(room_darkness, SDL_SRCALPHA, 
                                        (vanfire_window_health[0] +
                                         vanfire_window_health[1] +
                                         vanfire_window_health[2] +
                                         vanfire_window_health[3]) * 20);
                        src.x = 0;
                        src.y = 0;
                        src.w = LoP.tilesize * 20;
                        src.h = LoP.tilesize * 10;
                        dest.x = 0;
                        dest.y = LoP.tilesize * 2;
                        dest.w = src.w;
                        dest.h = src.h - (LoP.tilesize * 2);
                        SDL_BlitSurface(room_darkness, &src, screen, &dest);
                }
                // end render room darkness for VanFire
                // render room darkness for Tempus
                if (scene_number == 35 && !ed_dead.active) {
                        // Filling the surface with black
                        SDL_FillRect(room_darkness, NULL, SDL_MapRGB(end_screen->format, 0, 0, 0));
                        // set alpha
                        // use flame health to determine alpha
                        o = (((tempus_flame_health[0] +
                               tempus_flame_health[1] +
                               tempus_flame_health[2] +
                               tempus_flame_health[3]) * -64) + 256);
                        SDL_SetAlpha(room_darkness, SDL_SRCALPHA, o);
                        src.x = 0;
                        src.y = 0;
                        src.w = LoP.tilesize * 20;
                        src.h = LoP.tilesize * 10;
                        dest.x = 0;
                        dest.y = LoP.tilesize * 2;
                        dest.w = src.w;
                        dest.h = src.h - (LoP.tilesize * 2);
                        SDL_BlitSurface(room_darkness, &src, screen, &dest);
                }
                // end render room darkness
                // play wave audio if at ocean
                if (at_ocean.active) {
                        at_ocean.timer += delta_time;
                        //printf("ocean timer: %i\n", at_ocean.timer);
                }
                if (at_ocean.timer >= 4000) {
                        at_ocean.timer = 0;
                        // play waves sound
                        if (!LoP.mute) {
                                waves_channel = Mix_PlayChannel(-1, waves, 0);
                        }
                }
                // end play wave audio if at ocean

                // render header
                // set magenta as transparent
		colorkey = SDL_MapRGB(alphabet2->format, 255, 0, 255);
		/* We now enable this surface's colorkey and draw
		 * it again. To turn off the colorkey again, we would
		 * replace the SDL_SRCCOLORKEY flag with zero. */
		SDL_SetColorKey(alphabet2,
				SDL_SRCCOLORKEY | SDL_RLEACCEL,
				(Uint16) SDL_MapRGB(alphabet2->format, 255, 0, 255));
                // draw score
                if (score != old_score) {
                        // redraw the header
                        SDL_FillRect(header, NULL, SDL_MapRGB(header->format, 0, 0, 0));
                        // render score on screen
                        // rollover support if the score gets too high (max digits is 7)
                        if (score > 9999999) score = 0;
                        // let's use the unroll method
                        old_score = score;
                        o = score; // used here to hold the score minus the previous printed place values
                        header_x_pos = 0;
                        while (header_x_pos < TILESIZE * LoP.zoom) {
                                // millions place
                                for (i = -1; old_score >= 0; i++) {
                                        old_score -= 1000000;
                                }
                                o = o - (i * 1000000);
                                old_score = o;
                                // these are the same for all Blits below so I just declare them once
                                src.y = alphabet_width;
                                src.w = alphabet_width;
                                src.h = alphabet_width;
                                dest.w = 200 * LoP.zoom;
                                dest.h = 20 * LoP.zoom;
                                //

                                // millions place
                                //printf("%i million, old_score = %i\n", i, old_score);
                                src.x = (NUMBER_OFFSET * LoP.zoom) + (i * alphabet_width);
                                dest.x  = (HEADER_SCORE_OFFSET_X * LoP.zoom) + header_x_pos;
                                dest.y  = (HEADER_SCORE_OFFSET_Y * LoP.zoom);
                                SDL_BlitSurface(alphabet2, &src, header, &dest);

                                // hundred thousands place
                                for (i = -1; old_score >= 0; i++) {
                                        old_score -= 100000;
                                }
                                o = o - (i * 100000);
                                old_score = o;
                                header_x_pos += alphabet_width;
                                //printf("%i hundred thousand, old_score = %i\n", i, old_score);
                                src.x = (NUMBER_OFFSET * LoP.zoom) + (i * alphabet_width);
                                dest.x  = (HEADER_SCORE_OFFSET_X * LoP.zoom) + header_x_pos;
                                dest.y  = (HEADER_SCORE_OFFSET_Y * LoP.zoom);
                                SDL_BlitSurface(alphabet2, &src, header, &dest);

                                // ten thousands place
                                for (i = -1; old_score >= 0; i++) {
                                        old_score -= 10000;
                                }
                                o = o - (i * 10000);
                                old_score = o;
                                header_x_pos += alphabet_width;
                                //printf("%i ten thousand, old_score = %i\n", i, old_score);
                                src.x = (NUMBER_OFFSET * LoP.zoom) + (i * alphabet_width);
                                dest.x  = (HEADER_SCORE_OFFSET_X * LoP.zoom) + header_x_pos;
                                dest.y  = (HEADER_SCORE_OFFSET_Y * LoP.zoom);
                                SDL_BlitSurface(alphabet2, &src, header, &dest);

                                // thousands place
                                for (i = -1; old_score >= 0; i++) {
                                        old_score -= 1000;
                                }
                                o = o - (i * 1000);
                                old_score = o;
                                header_x_pos += alphabet_width;
                                //printf("%i thousand\n", i);
                                src.x = (NUMBER_OFFSET * LoP.zoom) + (i * alphabet_width);
                                dest.x  = (HEADER_SCORE_OFFSET_X * LoP.zoom) + header_x_pos;
                                dest.y  = (HEADER_SCORE_OFFSET_Y * LoP.zoom);
                                SDL_BlitSurface(alphabet2, &src, header, &dest);

                                // hundreds place
                                for (i = -1; old_score >= 0; i++) {
                                        old_score -= 100;
                                }
                                o = o - (i * 100);
                                old_score = o;
                                header_x_pos += alphabet_width;
                                //printf("%i hundred, old_score = %i\n", i, old_score);
                                src.x = (NUMBER_OFFSET * LoP.zoom) + (i * alphabet_width);
                                dest.x  = (HEADER_SCORE_OFFSET_X * LoP.zoom) + header_x_pos;
                                dest.y  = (HEADER_SCORE_OFFSET_Y * LoP.zoom);
                                SDL_BlitSurface(alphabet2, &src, header, &dest);

                                // tens place
                                for (i = -1; old_score >= 0; i++) {
                                        old_score -= 10;
                                }
                                o = o - (i * 10);
                                old_score = o;
                                header_x_pos += alphabet_width;
                                //printf("%i tens\n", i);
                                src.x = (NUMBER_OFFSET * LoP.zoom) + (i * alphabet_width);
                                dest.x  = (HEADER_SCORE_OFFSET_X * LoP.zoom) + header_x_pos;
                                dest.y  = (HEADER_SCORE_OFFSET_Y * LoP.zoom);
                                SDL_BlitSurface(alphabet2, &src, header, &dest);

                                // ones place
                                for (i = -1; old_score >= 0; i++) {
                                        old_score -= 1;
                                }
                                o = o - (i * 1);
                                old_score = o;
                                header_x_pos += alphabet_width;
                                //printf("%i ones\n", i);
                                src.x = (NUMBER_OFFSET * LoP.zoom) + (i * alphabet_width);
                                dest.x  = (HEADER_SCORE_OFFSET_X * LoP.zoom) + header_x_pos;
                                dest.y  = (HEADER_SCORE_OFFSET_Y * LoP.zoom);
                                SDL_BlitSurface(alphabet2, &src, header, &dest);
                        }
                        old_score = score;
                }
                // draw the "health" or three months worth of time
                src.y = 30 * LoP.zoom; // these stay the same
                src.w = LoP.tilesize;
                src.h = LoP.tilesize;
                dest.y = (HEADER_HEALTH_OFFSET_Y * LoP.zoom);
                dest.w = LoP.tilesize;
                dest.h = LoP.tilesize;
                //
                switch (ed.health) { // 12 possible combinations
                case 12: // 4/4, 4/4, 4/4
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 11: // 3/4, 4/4, 4/4
                        src.x = 10 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 10: // 2/4, 4/4, 4/4
                        src.x = 20 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 9: // 1/4, 4/4, 4/4
                        src.x = 30 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 8: // 0/4, 4/4, 4/4
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 7: // 0/4, 3/4, 4/4
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 10 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 6: // 0/4, 2/4, 4/4
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 20 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 5: // 0/4, 1/4, 4/4
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 30 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 4: // 0/4, 0/4, 4/4
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 0;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 3: // 0/4, 0/4, 3/4
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 10 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 2: // 0/4, 0/4, 2/4
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 20 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 1: // 0/4, 0/4, 1/4
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 30 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                case 0: // All hourglasses are empty
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (12 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        src.x = 40 * LoP.zoom;
                        dest.x = (HEADER_HEALTH_OFFSET_X * LoP.zoom) + (24 * LoP.zoom);
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                        break;
                default:
                        break;
                }
                // draw the pandora based on which bosses have been defeated
                // ___
                if (!boss.king_troll_dead && 
                    !boss.vanfire_dead && 
                    !boss.tempus_dead) {
                        src.x = 0;
                        src.y = 40 * LoP.zoom;
                        src.w = 20 * LoP.zoom;
                        src.h = 20 * LoP.zoom;
                        dest.x  = 85 * LoP.zoom;
                        dest.y = LoP.zoom;
                        dest.w = 20 * LoP.zoom;
                        dest.h = 20 * LoP.zoom;
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                }
                // __| 
                if (!boss.king_troll_dead && 
                    !boss.vanfire_dead && 
                     boss.tempus_dead) {
                        src.x = 20 * LoP.zoom;
                        src.y = 40 * LoP.zoom;
                        src.w = 20 * LoP.zoom;
                        src.h = 20 * LoP.zoom;
                        dest.x  = 85 * LoP.zoom;
                        dest.y = LoP.zoom;
                        dest.w = 20 * LoP.zoom;
                        dest.h = 20 * LoP.zoom;
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                }
                // _)_ 
                if (!boss.king_troll_dead && 
                     boss.vanfire_dead && 
                    !boss.tempus_dead) {
                        src.x = 40 * LoP.zoom;
                        src.y = 40 * LoP.zoom;
                        src.w = 20 * LoP.zoom;
                        src.h = 20 * LoP.zoom;
                        dest.x  = 85 * LoP.zoom;
                        dest.y = LoP.zoom;
                        dest.w = 20 * LoP.zoom;
                        dest.h = 20 * LoP.zoom;
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                }
                // _)| 
                if (!boss.king_troll_dead && 
                     boss.vanfire_dead && 
                     boss.tempus_dead) {
                        src.x = 60 * LoP.zoom;
                        src.y = 40 * LoP.zoom;
                        src.w = 20 * LoP.zoom;
                        src.h = 20 * LoP.zoom;
                        dest.x  = 85 * LoP.zoom;
                        dest.y = LoP.zoom;
                        dest.w = 20 * LoP.zoom;
                        dest.h = 20 * LoP.zoom;
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                }
                // (__
                if ( boss.king_troll_dead && 
                    !boss.vanfire_dead && 
                    !boss.tempus_dead) {
                        src.x = 80 * LoP.zoom;
                        src.y = 40 * LoP.zoom;
                        src.w = 20 * LoP.zoom;
                        src.h = 20 * LoP.zoom;
                        dest.x = 85 * LoP.zoom;
                        dest.y = LoP.zoom;
                        dest.w = 20 * LoP.zoom;
                        dest.h = 20 * LoP.zoom;
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                }
                // (_|
                if ( boss.king_troll_dead && 
                    !boss.vanfire_dead && 
                     boss.tempus_dead) {
                        src.x = 100 * LoP.zoom;
                        src.y = 40 * LoP.zoom;
                        src.w = 20 * LoP.zoom;
                        src.h = 20 * LoP.zoom;
                        dest.x = 85 * LoP.zoom;
                        dest.y = LoP.zoom;
                        dest.w = 20 * LoP.zoom;
                        dest.h = 20 * LoP.zoom;
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                }
                // ()_
                if ( boss.king_troll_dead && 
                     boss.vanfire_dead && 
                    !boss.tempus_dead) {
                        src.x = 120 * LoP.zoom;
                        src.y = 40 * LoP.zoom;
                        src.w = 20 * LoP.zoom;
                        src.h = 20 * LoP.zoom;
                        dest.x = 85 * LoP.zoom;
                        dest.y = LoP.zoom;
                        dest.w = 20 * LoP.zoom;
                        dest.h = 20 * LoP.zoom;
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                }
                // ()|
                if ( boss.king_troll_dead && 
                     boss.vanfire_dead && 
                     boss.tempus_dead) {
                        src.x = 140 * LoP.zoom;
                        src.y = 40 * LoP.zoom;
                        src.w = 20 * LoP.zoom;
                        src.h = 20 * LoP.zoom;
                        dest.x = 85 * LoP.zoom;
                        dest.y = LoP.zoom;
                        dest.w = 20 * LoP.zoom;
                        dest.h = 20 * LoP.zoom;
                        SDL_BlitSurface(alphabet2, &src, header, &dest);
                }
                // finish rendering the header to the screen
                src.x = 0;
                src.y = 0;
                src.w = LoP.zoom * 200;
                src.h = 20 * LoP.zoom;
                dest.x  = 0;
                dest.y  = 0;
                dest.w = LoP.zoom * 200;
                dest.h = 20 * LoP.zoom;
                SDL_BlitSurface(header, &src, screen, &dest);
                // end render header
		// Ask SDL to update the entire screen.
		SDL_Flip(screen);

		// give the processor some time to itself.
		SDL_Delay(8);

		// set time to old_time
		old_time = time;

                // Quit the game
                if (quit) {
                        //printf ("game quit\n");
                        // print our time if LoP.track_time is active
                        if (LoP.track_time) {
                                printf ("Time played: %i:%.2i:%.2i:%.4i\n", hours, minutes, seconds, milliseconds); 
                        }
                        running = FALSE;
                        // Free bitmap memory
                        SDL_FreeSurface(title);
                        SDL_FreeSurface(title2);
                        SDL_FreeSurface(intro);
                        SDL_FreeSurface(intro2);
                        SDL_FreeSurface(terrain);
                        SDL_FreeSurface(terrain2);
                        SDL_FreeSurface(bosses);
                        SDL_FreeSurface(bosses2);
                        SDL_FreeSurface(characters);
                        SDL_FreeSurface(characters2);
                        SDL_FreeSurface(alphabet2);
                        SDL_FreeSurface(header);
                        SDL_FreeSurface(sceneSurface);
                        SDL_FreeSurface(end_screen);
                        SDL_FreeSurface(room_darkness);
                        SDL_FreeSurface(screen);
                        // free our audio.
                        SDL_PauseAudio(1);
                        SDL_LockAudio();
                        Mix_FreeMusic(intro_theme);
                        Mix_FreeMusic(overworld);
                        Mix_FreeMusic(underworld);
                        Mix_FreeMusic(boss_theme);
                        Mix_FreeChunk(ed_swish);
                        Mix_FreeChunk(ed_pain);
                        Mix_FreeChunk(troll_pain);
                        Mix_FreeChunk(bat);
                        Mix_FreeChunk(rat);
                        Mix_FreeChunk(steps_down);
                        Mix_FreeChunk(steps_up);
                        Mix_FreeChunk(win);
                        Mix_FreeChunk(rock);
                        Mix_FreeChunk(flame);
                        Mix_FreeChunk(shake);
                        Mix_FreeChunk(king_troll_pain);
                        Mix_FreeChunk(tink);
                        Mix_FreeChunk(waves);
                        SDL_UnlockAudio();
                        // kill SDL
                        SDL_Quit();
                        // quit the program
                        return 0;
                }
                // end Quit the game

		/*
		 *
		 *
		 * stuff for game goes here ^^^ */
	}
	return 0;
}
